<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BiayaInstalasiModel;
use Carbon\Carbon;

class BiayaInstalasi extends Controller
{
    protected $ModelBiayaInstalasi;
    function __construct()
	{
		$this->ModelBiayaInstalasi = new BiayaInstalasiModel();
	}

	public function biayainstalasi(){

		$list_biaya_instalasi = $this->ModelBiayaInstalasi->listBiayaInstalasi();
    			
    	return view('biaya_instalasi.biayainstalasi',['list_biaya_instalasi' => $list_biaya_instalasi]);
	}

	//tambah
	public function simpan(Request $request){
        $this->ModelBiayaInstalasi->tambah($request);
        return redirect('/biaya-instalasi');
    }

	public function update(Request $request){
    	$this->ModelBiayaInstalasi->prosesUpdate($request);
    			
    	return redirect('/biaya-instalasi');
    }

    public function hapus($id){
    	$this->ModelBiayaInstalasi->hapus($id);
    			
    	return redirect('/biaya-instalasi');
    }
}
