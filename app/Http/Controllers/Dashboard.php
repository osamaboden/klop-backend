<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DashboardModel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class Dashboard extends Controller
{
    
	protected $ModelDashboard;
	function __construct()
	{
		$this->ModelDashboard = new DashboardModel();
	}

    public function index(){
    	$dologin=array('alert' => 'kosong');
    	return view('login',['dologin' => $dologin]);
    }


    public function ceklogin(Request $request){
    	$dologin = $this->ModelDashboard->cekuser($request);
    	//print_r($dologin);
    	//echo $dologin['alert'];
    	
    	
	    	if ($dologin['alert'] == 'sukses'){
	    		return redirect('/home');
	    	
	    	}else{
	    		
	    		return view('login',['dologin' => $dologin]);
	    	}

    		
    }

    public function logout(Request $request){
    	$request->session()->forget('id');
    	$request->session()->forget('nama');
    	$request->session()->forget('hak');
    	
    	$dologin=array('alert' => 'kosong');
		return redirect('/');

    }

    public function home(Request $request){
    	if($request->session()->has('nama','id','hak')){
    		
            $listorder = $this->ModelDashboard->ListOrder();
            $listpekerjaan = $this->ModelDashboard->listPekerjaan();
            

            return view('dashboard',['listorder' => $listorder, 'listpekerjaan' => $listpekerjaan]);
    		//menampilkan session
			//echo $request->session()->get('id');
			//echo " ".$request->session()->get('nama');
			//echo " ".$request->session()->get('hak');
		}else{
			echo 'Tidak ada data dalam session.';
			
			return redirect('/');
		}
    	
    }

}

