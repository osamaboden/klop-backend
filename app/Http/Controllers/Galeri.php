<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GaleriModel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class Galeri extends Controller
{
    protected $ModelGaleri;
	function __construct()
	{
		$this->ModelGaleri = new GaleriModel();
	}

	public function galeri(){

		if(session()->has('nama','id','hak')){

           $listgaleri = $this->ModelGaleri->listGaleri();
           $detilharga_produk = array();
           $detilgaleri_produk = $this->ModelGaleri->detilProdukGaleri();
    			
    	   return view('galeri.galeri',['listgaleri' => $listgaleri, 'detilharga_produk' => $detilharga_produk, 'detilgaleri_produk' => $detilgaleri_produk ]);
        
        }else{
            return redirect('/');
        }

	}

}
