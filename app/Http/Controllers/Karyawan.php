<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KaryawanModel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class Karyawan extends Controller
{
    function __construct()
	{
		$this->ModelKaryawan = new KaryawanModel();
	}

    public function index(){
    	$listkaryawan = $this->ModelKaryawan->listKaryawan();
    	$listdivisi = $this->ModelKaryawan->listDivisi();
    			
    	return view('karyawan.karyawan',['listkaryawan' => $listkaryawan, 'listdivisi' => $listdivisi]);
    }

    public function simpan(Request $request){
    	
    	$this->ModelKaryawan->tambahKaryawan($request);
    	return redirect('/karyawan');	
    	
    }

    public function update(Request $request){
    	
    	$this->ModelKaryawan->prosesUpdateKaryawan($request);
    	return redirect('/karyawan');	
    	
    }

    public function hapus($id_karyawan){
    	
    	$this->ModelKaryawan->hapusKaryawan($id_karyawan);
    	return redirect('/karyawan');	
    	
    }


    public function detil($id){
        $datakaryawan = $this->ModelKaryawan->singleKaryawan($id);
        $alamatmember = array(); //$this->ModelKaryawan->listOrderDetilMember($id);
        $pekerjaankaryawan = $this->ModelKaryawan->listPekerjaanByKaryawan($id); 
        $listorder = $this->ModelKaryawan->listOrderDetilKaryawan($id);
            
        return view('karyawan.detilkaryawan',['datakaryawan' => $datakaryawan,'alamatmember' => $alamatmember, 'pekerjaankaryawan' => $pekerjaankaryawan, 'listorder' => $listorder]);
    }


    //===============================daftar perkerjaan agen / karyawan==============================================
    public function daftarpekerjaan(){
    	$listpekerjaan = $this->ModelKaryawan->listPekerjaan();
        $listkaryawan = $this->ModelKaryawan->listKaryawan();
        $listorder = $this->ModelKaryawan->listOrder();
    	$listdivisi = array();
    	return view('karyawan.pekerjaan',['listpekerjaan' => $listpekerjaan, 'listkaryawan' => $listkaryawan, 'listorder' => $listorder, 'listdivisi' => $listdivisi]);
    }

    public function simpanpekerjaan(Request $request){
        
        $waktu = $this->ModelKaryawan->tambahPekerjaan($request);
        return redirect('/daftarpekerjaan');
    }

    public function updatepekerjaan(Request $request){
        $this->ModelKaryawan->prosesUpdatePekerjaan($request);
        return redirect('/daftarpekerjaan');
    }

    public function hapuspekerjaan($id){
        $this->ModelKaryawan->hapusPekerjaan($id);
        return redirect('/daftarpekerjaan');
    }

    public function detilpekerjaan ($id){
        $detilpekerjaan = $this->ModelKaryawan->detilPekerjaan($id);
        $id_order = $detilpekerjaan->id_order;
        $id_agen = $detilpekerjaan->id_agen;
        $galeri_pekerjaan_agen = $this->ModelKaryawan->galeriPekerjaanAgen($id_order, $id_agen);
       
        return view('karyawan.detilpekerjaan',['detilpekerjaan' => $detilpekerjaan, 'galeri_pekerjaan_agen' => $galeri_pekerjaan_agen]);
    }

    //===============================daftar perkerjaan agen / karyawan==============================================


    //============================divisi karyawan========================================================
    public function divisikaryawan(){

        $listdivisikaryawan = $this->ModelKaryawan->listDivisiKaryawan();
                
        return view('karyawan.karyawan_divisi',['listdivisikaryawan' => $listdivisikaryawan]);
    }

    //tambah
    public function simpandivisi(Request $request){
        $this->ModelKaryawan->tambahDivisiKaryawan($request);
        return redirect('/karyawandivisi');
    }

    public function updatedivisi(Request $request){
        $this->ModelKaryawan->prosesUpdateDivisiKaryawan($request);
                
        return redirect('/karyawandivisi');
    }

    public function hapusdivisi($id){
        $this->ModelKaryawan->hapusDivisiKaryawan($id);
                
        return redirect('/karyawandivisi');
    }
    //============================divisi karyawan========================================================

    
}
