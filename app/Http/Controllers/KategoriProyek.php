<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KategoriProyekModel;
use Carbon\Carbon;

class KategoriProyek extends Controller
{
    protected $ModelKategoriProyek;
    function __construct()
	{
		$this->ModelKategoriProyek = new KategoriProyekModel();
	}

	public function kategoriproyek(){

		$listkategoriproyek = $this->ModelKategoriProyek->listKategoriProyek();
    			
    	return view('kategori_proyek.kategoriproyek',['listkategoriproyek' => $listkategoriproyek]);
	}

	//tambah
	public function simpan(Request $request){
        $this->ModelKategoriProyek->tambahKategoriProyek($request);
        return redirect('/proyekkategori');
    }

	public function update(Request $request){
    	$this->ModelKategoriProyek->prosesUpdate($request);
    			
    	return redirect('/proyekkategori');
    }

    public function hapus($id){
    	$this->ModelKategoriProyek->hapusKategoriProyek($id);
    			
    	return redirect('/proyekkategori');
    }
}
