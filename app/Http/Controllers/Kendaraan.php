<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KendaraanModel;
use Carbon\Carbon;

class Kendaraan extends Controller
{
    protected $ModelKendaraan;
    function __construct()
	{
		$this->ModelKendaraan = new KendaraanModel();
	}

	public function kendaraan(){

		$listkendaraan = $this->ModelKendaraan->listKendaraan();
    			
    	return view('kendaraan.kendaraan',['listkendaraan' => $listkendaraan]);
	}

	//tambah
	public function simpan(Request $request){
        $this->ModelKendaraan->tambah($request);
        return redirect('/kendaraan');
    }

	public function update(Request $request){
    	$this->ModelKendaraan->prosesUpdate($request);
    			
    	return redirect('/kendaraan');
    }

    public function hapus($id){
    	$this->ModelKendaraan->hapus($id);
    			
    	return redirect('/kendaraan');
    }
}
