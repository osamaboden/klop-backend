<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KontenModel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class Konten extends Controller
{
    function __construct()
	{
		$this->ModelKonten = new KontenModel();
	}

	public function konten(){
        if(session()->has('nama','id','hak','foto')){

    	$listkonten = $this->ModelKonten->listKonten();
    	return view('konten.konten',['listkonten' => $listkonten]);

        }else{
            return redirect('/');
        }

    }

    public function editKonten($id){
        if(session()->has('nama','id','hak','foto')){

    	$editkonten = $this->ModelKonten->editKonten($id);
    	return view('konten.edit_konten',['editkonten' => $editkonten]);

        }else{
            return redirect('/');
        }

    }

    public function simpanKonten(Request $request){
        if(session()->has('nama','id','hak','foto')){

    		$updatekonten = $this->ModelKonten->simpanKonten($request);

    		return redirect('/konten');

        }else{
            return redirect('/');
        }

    }

}
