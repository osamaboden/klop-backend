<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LaporanModel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class Laporan extends Controller
{
    function __construct()
	{
		$this->ModelLaporan = new LaporanModel();
	}

	public function laporanpekerjaan(){
        if(session()->has('nama','id','hak','foto')){

    	$listpekerjaan = $this->ModelLaporan->listPekerjaan();
    	return view('laporan.laporan_pekerjaan',['listpekerjaan' => $listpekerjaan]);

        }else{
            return redirect('/');
        }

    }

    
    public function cariByKaryawan(Request $request){
        if($request->session()->has('nama','id','hak','foto')){

            $listpekerjaan = $this->ModelLaporan->listPekerjaanByKaryawan($request);

            return view('laporan.laporan_pekerjaan',['listpekerjaan' => $listpekerjaan]);

        }else{
            return redirect('/');
        }
    
    }


    public function cariByTanggal(Request $request){
        if($request->session()->has('nama','id','hak','foto')){

            $tanggal = str_replace(" ","",$request->bytanggal);
            $explode = explode('-',$tanggal);
            $jadiiapa = $explode[0];
            $awal = explode('/',$explode[0]);
            $tgl_mulai = $awal[2]."-".$awal[1]."-".$awal[0]." 00:00:00";
            $mulai = $awal[2]."-".$awal[1]."-".$awal[0];
            
            $akhir = explode('/',$explode[1]);
            $tgl_selesai = $akhir[2]."-".$akhir[1]."-".$akhir[0]." 23:59:00";
            $selesai = $akhir[2]."-".$akhir[1]."-".$akhir[0];

            $mulai = date("d M Y",strtotime($mulai));
            $selesai = date("d M Y",strtotime($selesai));


            $listpekerjaan = $this->ModelLaporan->listPekerjaanByTanggal($tgl_mulai, $tgl_selesai);

            //print_r($tgl_selesai);

            return view('laporan.laporan_pekerjaan',['listpekerjaan' => $listpekerjaan, 'mulai'=>$mulai, 'selesai'=>$selesai]);

        }else{
            return redirect('/');
        }
    
    }


    public function laporanOrder(){
        if(session()->has('nama','id','hak','foto')){

            $listorder = $this->ModelLaporan->listOrder();
            $listkategori_proyek = $this->ModelLaporan->listKategoriProyek();
            return view('laporan.laporan_order',['listorder' => $listorder, 'listkategori_proyek' => $listkategori_proyek]);
        
        }else{
            return redirect('/');
        }
        
    }

    public function searchByTanggal(Request $request){
        if(session()->has('nama','id','hak','foto')){

            $tanggal = str_replace(" ","",$request->bytanggal);
            $explode = explode('-',$tanggal);
            $jadiiapa = $explode[0];
            $awal = explode('/',$explode[0]);
            $tgl_mulai = $awal[2]."-".$awal[1]."-".$awal[0]." 00:00:00";
            $mulai = $awal[2]."-".$awal[1]."-".$awal[0];
            
            $akhir = explode('/',$explode[1]);
            $tgl_selesai = $akhir[2]."-".$akhir[1]."-".$akhir[0]." 23:59:00";
            $selesai = $akhir[2]."-".$akhir[1]."-".$akhir[0];

            $mulai = date("d M Y",strtotime($mulai));
            $selesai = date("d M Y",strtotime($selesai));

            $listorder = $this->ModelLaporan->listOrderByTanggal($tgl_mulai, $tgl_selesai);
            $listkategori_proyek = $this->ModelLaporan->listKategoriProyek();

            return view('laporan.laporan_order',['listorder' => $listorder, 'mulai'=>$mulai, 'selesai'=>$selesai, 'listkategori_proyek' => $listkategori_proyek] );

        }else{

            return redirect('/');
        }
    
    }

    public function searchByJumlah(Request $request){
        if(session()->has('nama','id','hak','foto')){

            $mulai = "";
            $selesai = "";

            $listorder = $this->ModelLaporan->listOrderByJumlah($request);
            $listkategori_proyek = $this->ModelLaporan->listKategoriProyek();
            $keyword = "";
            
            return view('laporan.laporan_order',['listorder' => $listorder, 'mulai'=>$mulai, 'selesai'=>$selesai, 'listkategori_proyek' => $listkategori_proyek, 'keyword' => $keyword] );

        }else{

            return redirect('/');
        }
    
    }

    public function searchByKategori(Request $request){
        if(session()->has('nama','id','hak','foto')){

            $mulai = "";
            $selesai = "";

            $listorder = $this->ModelLaporan->listOrderByKategori($request);
            $listkategori_proyek = $this->ModelLaporan->listKategoriProyek();
            $keyword = $this->ModelLaporan->kategoriProyekKeyword($request);
            
            return view('laporan.laporan_order',['listorder' => $listorder, 'mulai'=>$mulai, 'selesai'=>$selesai, 'listkategori_proyek' => $listkategori_proyek, 'keyword' => $keyword] );

        }else{

            return redirect('/');
        }
    
    }


}
