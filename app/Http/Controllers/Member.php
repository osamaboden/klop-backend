<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MemberModel;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class Member extends Controller
{
    protected $ModelMember;
	function __construct()
	{
		$this->ModelMember = new MemberModel();
	}

	public function daftarmember(){
    	//mengambil data dari table produk
    	if(session()->has('nama','id','hak')){
            $listmember = $this->ModelMember->listMember();
    			
    	   return view('member.member',['listmember' => $listmember]);
        }else{
            return redirect('/');
        }
    }

    public function tambah(){
        return view('member.tambahmember');
    }

    public function simpan(Request $request){
        $this->ModelMember->tambahMember($request);
        return redirect('/member');
    }

    public function edit($id){
    	//mengambil data dari table produk berdasarkan id member
    	$datamember = $this->ModelMember->editMember($id);
    			
    	return view('member.editmember',['datamember' => $datamember]);
    }

    public function update(Request $request){
    	$this->ModelMember->prosesUpdate($request);
    			
    	return redirect('/member');
    }

    public function hapus($id){
    	$this->ModelMember->hapusMember($id);
    			
    	return redirect('/member');
    }

    public function detil($id){
        $datamember = $this->ModelMember->detilMember($id);
        $alamatmember = $this->ModelMember->alamatMember($id);
        $listorder = $this->ModelMember->listOrderDetilMember($id);

       // print_r($listorder);
                
        return view('member.detilmember',['datamember' => $datamember,'alamatmember' => $alamatmember, 'listorder' => $listorder]);
    }

    /*
    public function order(){
        $listorder = $this->ModelMember->listOrder();
               
        return view('member.order',['listorder' => $listorder]);
    }

    public function detilorder($id_order){
        $detilorder = $this->ModelMember->Order($id_order);
        $produkorder = $this->ModelMember->produkOrder($id_order);
        return view('member.detilorder',['detilorder' => $detilorder, 'produkorder' => $produkorder]);
    }
   */ 
}
