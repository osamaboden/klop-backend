<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderModel;
use Carbon\Carbon;

class Order extends Controller
{
    protected $ModelOrder;
	function __construct()
	{
		$this->ModelOrder = new OrderModel();
	}

    public function order(){
        if(session()->has('nama','id','hak','foto')){

            $listorder = $this->ModelOrder->listOrder();
            return view('member.order',['listorder' => $listorder]);
        
        }else{
            return redirect('/');
        }
        
    }

    public function detilorder($id_order){
        if(session()->has('nama','id','hak','foto')){
            $detilorder = $this->ModelOrder->Order($id_order);
            $produkorder = $this->ModelOrder->produkOrder($id_order);
            $orderstatus = $this->ModelOrder->orderStatus($id_order);
            $orderpembayaran = $this->ModelOrder->orderPembayaran($id_order);

            $listkaryawan = $this->ModelOrder->listKaryawan();

            return view('member.detilorder',['detilorder' => $detilorder, 'produkorder' => $produkorder, 'orderstatus'=>$orderstatus, 'orderpembayaran' => $orderpembayaran, 'listkaryawan' => $listkaryawan]);
        }else{
            return redirect('/');
        }
    }

    //menu konfirmasi pembayaran
	public function kofirmasipembayaran(){
        if(session()->has('nama','id','hak','foto')){
        	$konfirmPembayaran = $this->ModelOrder->konfirmasiPembayaran();
        	return view('order.konfirmasipembayaran',['konfirmPembayaran' => $konfirmPembayaran]);
        }else{
            return redirect('/');
        }
    }

    public function ubahkonfirmasi(Request $request){
    	
        if(session()->has('nama','id','hak','foto')){
            $konfirmPembayaran = $this->ModelOrder->prosesUbahKonfirmasi($request);
        	return redirect('/order/kofirmpembayaran');
        }else{
            return redirect('/');
        }
    }

    //detil konfirmasi pembayaran
    public function detilpembayaran($id_order){
       
        if(session()->has('nama','id','hak','foto')){
            $orderpembayaran = $this->ModelOrder->orderPembayaran($id_order);
            return view('order.detil_konfirmasipembayaran',['orderpembayaran' => $orderpembayaran]);

        }else{
            return redirect('/');
        }
    }
}
