<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\ProdukModel;

class Produk extends Controller
{
	protected $ModelProduk;
	function __construct()
	{
		$this->ModelProduk = new ProdukModel();
	}

    public function daftarproduk(){

    	//mengambil data dari table produk
    	$listproduk = $this->ModelProduk->listProduk();
    			
    	return view('produk.apaproduk',['listproduk' => $listproduk]);
    }

    public function edit($id){

    	//mengambil data dari table produk berdasarkan id produk
    	$dataproduk = $this->ModelProduk->editProduk($id);
        $produkharga = $this->ModelProduk->editProdukHarga($id);
        $produkgaleri = $this->ModelProduk->editProdukGaleri($id);
    	
    	return view('produk.editproduk',['dataproduk' => $dataproduk, 'produkharga' =>$produkharga, 'produkgaleri' =>$produkgaleri]);
    }

    public function update(Request $request){

    	$this->ModelProduk->prosesUpdate($request);
    			
    	return redirect('/product');
    }

    public function hapus($id){

    	$this->ModelProduk->hapusProduk($id);
    			
    	return redirect('/product');
    }

    public function tambah(){
        $dataproduk = array();
        return view('produk.tambahproduk');
    }

    /* fungsi untuk file tambaheditproduk.blade.php
    public function tambah(){
        $dataproduk = array();
    	return view('produk.tambaheditproduk',['dataproduk' => $dataproduk,'id' => '']);
    }
    */

    public function simpan(Request $request){

        $proo= $this->ModelProduk->tambahProduk($request);
        
        return redirect('/product');  
       
    }

    public function detilProduk($id_produk){

        $detilproduk = $this->ModelProduk->detilProduk($id_produk);
        $detilharga_produk = $this->ModelProduk->detilProdukHarga($id_produk);
        $detilgaleri_produk = $this->ModelProduk->detilProdukGaleri($id_produk);
        
        return view('produk.detilproduk',['detilproduk' => $detilproduk, 'detilharga_produk' => $detilharga_produk, 'detilgaleri_produk' => $detilgaleri_produk]);  
       
    }


}
