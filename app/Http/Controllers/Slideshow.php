<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SlideshowModel;
use Carbon\Carbon;

class Slideshow extends Controller
{
    protected $ModelSlideshow;
    function __construct()
	{
		$this->ModelSlideshow = new SlideshowModel();
	}

	public function index(){

		$listslideshow = $this->ModelSlideshow->listSlideshow();
    			
    	return view('slideshow.slideshow',['listslideshow' => $listslideshow]);
	}

	//tambah
	public function simpan(Request $request){
        $gambarku = $this->ModelSlideshow->tambah($request);
        //print_r($request->file('gambar'));
        return redirect('/slideshow');
    }

	public function update(Request $request){
    	$this->ModelSlideshow->prosesUpdate($request);
    			
    	return redirect('/slideshow');
    }

    public function hapus($id){
    	$this->ModelSlideshow->hapus($id);
    			
    	return redirect('/slideshow');
    }
}
