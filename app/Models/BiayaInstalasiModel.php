<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class BiayaInstalasiModel extends Model
{
    use HasFactory;

    public function listBiayaInstalasi(){
    	$list= DB::table('biaya_installation')
    			->where('status', '<>', 'delete')
    			->get();
    	return $list;
    }

    public function tambah($request){
        
      	DB::table('biaya_installation')->insert([
        'nama' => $request->nama,
        'min_qty_produk' => $request->min_qty,
		'max_qty_produk' => $request->max_qty,
		'harga' => $request->harga,
        'status' => $request->status
        ]);
    }

    public function prosesUpdate($request){
    	$list = DB::table('biaya_installation')->where('id',$request->id)->update([
		'nama' => $request->nama,
		'min_qty_produk' => $request->min_qty,
		'max_qty_produk' => $request->max_qty,
		'harga' => $request->harga,
        'status' => $request->status
		]);	
    }

    public function hapus($id){

    	$list = DB::table('biaya_installation')->where('id',$id)->update([
        'status' => 'delete'
		]);	
    }
}
