<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;


class DashboardModel extends Model
{
    use HasFactory;

    public function cekuser($request){
    	$hasil=array();

    	$no_hp = $request->input('no_telp');
    	$password = $request->input('password');

    	$data = DB::table('karyawan')
    		->select('id', 'nama', 'no_hp', 'password', 'hak_akses', 'foto_profile')
            ->where('no_hp', '=', $no_hp)
            ->where('hak_akses', '=', 'admin')
            ->where('status', '=', 'aktif')
            ->first();
            

        if(!is_null($data)){

        	 if($password !="" &&  Hash::check('01X'.$password.'@#', $data->password)){
        	 	//session
        	 	$hasil['alert'] = 'sukses';
        	 	$request->session()->put(['id'=>$data->id, 'nama'=>$data->nama, 'hak'=>$data->hak_akses, 'foto'=>$data->foto_profile]);
        	 }else{
        	 	$hasil['alert'] = 'Kata sandi salah';
        	 }

        }else{
        	$hasil['alert'] = 'Nomor telepon tidak terdaftar';
        }

    	return $hasil;
    }

    
    public function listOrder(){
        $hasil = [];
        $i = 0 ;

        $order = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*','member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->get();

        foreach ($order as $item) {
            $hasil[$i]['waktu_order'] =  $item->waktu_order;
            $hasil[$i]['id'] =  $item->id;
            $hasil[$i]['nama_member'] =  $item->nama_member;
            $hasil[$i]['delivery'] =  $item->delivery;
            
            $status_order = DB::table('order_status')
            ->select('status as order_status')
            ->where('id_order', $item->id)
            ->orderBy('waktu_perubahan', 'DESC')
            ->offset(0)
            ->limit(1)
            ->first();

            if(!empty($status_order->order_status)){
                $hasil[$i]['status'] =  $status_order->order_status;
            }else{
                $hasil[$i]['status'] ="-";
            }
            
            $i++;
        }

        return $hasil;
    }

    public function listPekerjaan(){
        $list = DB::table('order_penugasan')
            ->leftJoin('karyawan', 'karyawan.id', '=', 'order_penugasan.id_karyawan')
            ->leftJoin('order', 'order.id', '=', 'order_penugasan.id_order')
            //->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            //->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            //->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            //->select('member_alamat.*', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            //->where('member_alamat.id_member',$id)
            ->where('order_penugasan.status','<>','delete')
            ->where('order_penugasan.status','=','baru')
            ->select('order_penugasan.*', 'karyawan.nama as nama_karyawan', 'order.waktu_order', 'order.delivery', 'order.installation')
            ->get();

        return $list;
    }
}
