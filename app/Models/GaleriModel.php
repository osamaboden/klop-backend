<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatabl;
use Illuminate\Support\Facades\Auth;
use Helper as helper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Collection;

class GaleriModel extends Model
{
    use HasFactory;

    public function listGaleri(){
    	$list= DB::table('member')
            ->where('status', '<>', 'delete')
            ->get();
    	return $list;
    }

    //hapus iki ming dinggo nyoba
    public function detilProdukGaleri(){
        $list= DB::table('galeri')
        		->where('status','aktif')
                ->get();
        return $list;
    }
}
