<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatabl;
use Illuminate\Support\Facades\Auth;
use Helper as helper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Collection;

class KaryawanModel extends Model
{
    use HasFactory;
    
    public function listKaryawan(){
    	$list = DB::table('karyawan')
            ->leftJoin('karyawan_divisi', 'karyawan_divisi.id', '=', 'karyawan.divisi')
            ->where('karyawan.status','<>','delete')
            ->select('karyawan.*', 'karyawan_divisi.nama as nama_divisi', 'karyawan_divisi.id as id_divisi' )
            ->get();

        return $list;
    }

    public function listDivisi(){
    	$list = DB::table('karyawan_divisi')->get();

        return $list;
    }

    public function tambahKaryawan($request){
	    DB::table('karyawan')->insert([
	        'nama' => $request->nama,
	        'no_hp' => $request->nohp,
	        'email' => $request->email,
	        'password' => Hash::make('01X'.$request->password.'@#'),
	        'divisi' => $request->divisi,
	        'hak_akses' => $request->hak_akses,
	        'status' => $request->status
	        ]);
   	}



    public function prosesUpdateKaryawan($request){
	    $password = $request->password;
	    if(!empty($password)){

	    	DB::table('karyawan')->where('id',$request->id)->update([
	        'nama' => $request->nama,
	        'no_hp' => $request->nohp,
	        'email' => $request->email,
	        'password' => $password,
	        'divisi' => $request->divisi,
	        'hak_akses' => $request->hak_akses,
	        'status' => $request->status
	        ]);
	    
	    }else{
	    	
	    	DB::table('karyawan')->where('id',$request->id)->update([
	        'nama' => $request->nama,
	        'no_hp' => $request->nohp,
	        'email' => $request->email,
	        'divisi' => $request->divisi,
	        'hak_akses' => $request->hak_akses,
	        'status' => $request->status
	        ]);
	   	}
   	}


   	public function hapusKaryawan($id_karyawan){
	    DB::table('karyawan')->where('id',$id_karyawan)->update([
	        'status' => 'delete'
	        ]);
   	}




   	//================================= jangan lupa di edit belum selesai
   	public function detilMember($id){
        $list = DB::table('member')->where('id',$id)->first();
        return $list;
    }

    public function alamatMember($id){
        $list = DB::table('member_alamat')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('member_alamat.*', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->where('member_alamat.id_member',$id)
            ->get();

        return $list;
    }


    public function listOrderDetilKaryawan($id_member){
        $hasil = array();
        $i = 0 ;
        $orderan = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*', 'member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->where('order.id_member',$id_member)
            ->get();


        foreach ($orderan as $item) {
          $hasil['order'][$i]['id_order'] =  $item->id;
          $hasil['order'][$i]['waktu_order'] =  $item->waktu_order;
          $hasil['order'][$i]['alamat'] =  $item->alamat;
          $hasil['order'][$i]['nama_kecamatan'] =  $item->nama_kecamatan;
          $hasil['order'][$i]['nama_kabupaten'] =  $item->nama_kabupaten;
          $hasil['order'][$i]['nama_provinsi'] =  $item->nama_provinsi;
          $hasil['order'][$i]['delivery'] =  $item->delivery;
          $hasil['order'][$i]['request_ready'] =  $item->request_ready;

          $produk = DB::table('order_produk')
            ->leftJoin('produk', 'produk.id', '=', 'order_produk.id_produk')
            ->select('order_produk.*', 'produk.nama as nama_produk')
            ->where('order_produk.id_order',$item->id)
            ->get();

          $j = 0;
          foreach ($produk as $row) {
             $hasil['order'][$i]['produk'][$j]['nomor_order'] = $row->id_order;
             $hasil['order'][$i]['produk'][$j]['nama_produk'] = $row->nama_produk;
             $hasil['order'][$i]['produk'][$j]['qty'] = $row->qty; 
             $hasil['order'][$i]['produk'][$j]['harga_satuan'] = $row->harga_total;
             $hasil['order'][$i]['produk'][$j]['diskon_satuan'] = $row->diskon;
             $hasil['order'][$i]['produk'][$j]['satuan_diskon'] = $row->satuan_diskon;
          $j++;
          }

          $i++;
        }
        return $hasil;
    }


    public function singleKaryawan($id){
    	$list = DB::table('karyawan')
            ->leftJoin('karyawan_divisi', 'karyawan_divisi.id', '=', 'karyawan.divisi')
            //->where('karyawan.status','<>','delete')
            ->where('karyawan.id',$id)
            ->select('karyawan.*', 'karyawan_divisi.nama as nama_divisi', 'karyawan_divisi.id as id_divisi' )
            ->first();

        return $list;
    }

    public function listPekerjaanByKaryawan($id){
    	$list = DB::table('order_penugasan')
    		->leftJoin('karyawan', 'karyawan.id', '=', 'order_penugasan.id_karyawan')
    		->leftJoin('order', 'order.id', '=', 'order_penugasan.id_order')
            //->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            //->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            //->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            //->select('member_alamat.*', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->where('order_penugasan.id_karyawan',$id)
            ->where('order_penugasan.status', 'selesai')
            ->select('order_penugasan.*', 'karyawan.nama as nama_karyawan', 'order.waktu_order')
            ->get();

        return $list;
    }



    //=============================== daftar pekerjaan agen / karyawan =================================
    // daftar pekerjaan agen / karyawan
    public function listPekerjaan(){
    	$list = DB::table('order_penugasan')
    		->leftJoin('karyawan', 'karyawan.id', '=', 'order_penugasan.id_karyawan')
    		->leftJoin('order', 'order.id', '=', 'order_penugasan.id_order')
            //->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            //->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            //->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            //->select('member_alamat.*', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            //->where('member_alamat.id_member',$id)
            ->where('order_penugasan.status','<>','delete')
            ->select('order_penugasan.*', 'karyawan.nama as nama_karyawan', 'order.waktu_order', 'order.delivery', 'order.installation')
            ->get();

        return $list;
    }

    public function listOrder(){
      $list = DB::table('order')
            ->select('order.id as id_order')
            ->get();

        return $list;
    }

    public function tambahPekerjaan($request){
      $waktu = $request->input('waktu_penugasan');
      $waktu_penugasan = date('Y-m-d H:i:s', strtotime($waktu));

      DB::table('order_penugasan')->insert([
        'id_karyawan' => $request->karyawan,
        'id_order' => $request->id_order,
        'kategori' => $request->kategori,
        'waktu_penugasan' => $waktu_penugasan,
        'status' => $request->status
        ]);
    }

    public function prosesUpdatePekerjaan($request){
      $waktu = $request->input('waktu_penugasan_edit');
      $waktu_penugasan = date('Y-m-d H:i:s', strtotime($waktu));
      
      $list = DB::table('order_penugasan')->where('id',$request->id)->update([
        'id_karyawan' => $request->karyawan,
        'id_order' => $request->id_order,
        'kategori' => $request->kategori,
        'waktu_penugasan' => $waktu_penugasan,
        'status' => $request->status
      ]);

        $id_admin = session()->get('id');
        if($request->kategori == 'delivery' && $request->status == 'dikerjakan' && !empty($request->id_order_hidden)){

            DB::table('order_status')->insert([
            'id_order' => $request->id_order_hidden,
            'waktu_perubahan' => date('Y-m-d H:i:s'),
            'status' => 'dikirim',
            'id_admin' => $id_admin
            ]);

        }else if($request->kategori == 'installation' && $request->status == 'dikerjakan' && !empty($request->id_order_hidden)){

            DB::table('order_status')->insert([
            'id_order' => $request->id_order_hidden,
            'waktu_perubahan' => date('Y-m-d H:i:s'),
            'status' => 'sedang dipasang',
            'id_admin' => $id_admin
            ]);
        
        }else if($request->kategori == 'delivery' && $request->status == 'selesai' && !empty($request->id_order_hidden)){

            DB::table('order_status')->insert([
            'id_order' => $request->id_order_hidden,
            'waktu_perubahan' => date('Y-m-d H:i:s'),
            'status' => 'sampai ditujuan',
            'id_admin' => $id_admin
            ]);
        
        }else if($request->kategori == 'installation' && $request->status == 'selesai' && !empty($request->id_order_hidden)){

            DB::table('order_status')->insert([
            'id_order' => $request->id_order_hidden,
            'waktu_perubahan' => date('Y-m-d H:i:s'),
            'status' => 'selesai',
            'id_admin' => $id_admin
            ]);
        }

    }

    public function hapusPekerjaan($id){
      $list = DB::table('order_penugasan')->where('id',$id)->update([
        'status' => 'delete'
      ]); 
    }

    
    public function detilPekerjaan($id){
      $list = DB::table('order_penugasan')
            ->leftJoin('karyawan', 'karyawan.id', '=', 'order_penugasan.id_karyawan')
            ->leftJoin('order', 'order.id', '=', 'order_penugasan.id_order')
            ->where('order_penugasan.status','<>','delete')
            ->where('order_penugasan.id','=',$id)
            ->select('order_penugasan.*', 'karyawan.id as id_agen', 'karyawan.nama as nama_karyawan', 'order.waktu_order', 'order.delivery', 'order.installation')
            ->first();

        return $list;
    }

    
    public function galeriPekerjaanAgen($id_order, $id_agen){
      $list = DB::table('galeri')
            ->where('id_order','=',$id_order)
            ->where('id_agen', '=',$id_agen)
            ->select('title', 'tipe', 'nama_file', 'thumbnail')
            ->get();

        return $list;
    }

    //=============================== daftar pekerjaan agen / karyawan =================================


    //====================================== divisi karyawan ========================================
    public function listdivisikaryawan(){
      $list= DB::table('karyawan_divisi')
          ->where('status', '<>', 'delete')
          ->get();
      return $list;
    }
    
    public function tambahDivisiKaryawan($request){
        
        DB::table('karyawan_divisi')->insert([
        'nama' => $request->nama,
        'status' => $request->status
        ]);
    }

    public function prosesUpdateDivisiKaryawan($request){
      $list = DB::table('karyawan_divisi')->where('id',$request->id)->update([
        'nama' => $request->nama,
        'status' => $request->status
      ]); 
    }

    public function hapusDivisiKaryawan($id){
      
      /*$list = DB::table('proyek_kategori')->where('id',$id)->delete();
      return $list;*/

      $list = DB::table('karyawan_divisi')->where('id',$id)->update([
        'status' => 'delete'
    ]); 
    }
    //====================================== divisi karyawna ========================================

}
