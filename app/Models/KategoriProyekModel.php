<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class KategoriProyekModel extends Model
{
    use HasFactory;

    
    public function listKategoriProyek(){
    	$list= DB::table('proyek_kategori')
    			->where('status', '<>', 'delete')
    			->get();
    	return $list;
    }

    public function tambahKategoriProyek($request){
        
      	DB::table('proyek_kategori')->insert([
        'nama' => $request->nama,
        'status' => $request->status
        ]);
    }

    public function prosesUpdate($request){
    	$list = DB::table('proyek_kategori')->where('id',$request->id)->update([
		'nama' => $request->nama,
        'status' => $request->status
		]);	
    }

    public function hapusKategoriProyek($id){
    	
    	/*$list = DB::table('proyek_kategori')->where('id',$id)->delete();
    	return $list;*/

    	$list = DB::table('proyek_kategori')->where('id',$id)->update([
        'status' => 'delete'
		]);	
    }
}
