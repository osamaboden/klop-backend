<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class KendaraanModel extends Model
{
    use HasFactory;

    public function listKendaraan(){
    	$list= DB::table('kendaraan')
    			->where('status', '<>', 'delete')
    			->get();
    	return $list;
    }

    public function tambah($request){
        
      	DB::table('kendaraan')->insert([
        'nama' => $request->nama,
        'merk_tipe' => $request->merk,
        'nopol' => $request->no_polisi,
        'kapasitas' => $request->kapasitas,
        'status' => $request->status
        ]);
    }

    public function prosesUpdate($request){
    	$list = DB::table('kendaraan')->where('id',$request->id)->update([
		'nama' => $request->nama,
        'merk_tipe' => $request->merk,
        
        'nopol' => $request->no_polisi,
        'kapasitas' => $request->kapasitas,
        'status' => $request->status
		]);	
    }

    public function hapus($id){

    	$list = DB::table('kendaraan')->where('id',$id)->update([
        'status' => 'delete'
		]);	
    }
}
