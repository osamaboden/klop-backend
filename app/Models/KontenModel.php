<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class KontenModel extends Model
{
    use HasFactory;

    public function listKonten(){

        $list= DB::table('konten')
                ->get();
        return $list;
        
    }

    public function editKonten($id){

        $list= DB::table('konten')
        		->where('id',$id)
                ->first();
        return $list;
        
    }

    public function simpanKonten($request){

        DB::table('konten')->where('id',$request->id)->update([
        'title' => $request->judul,
        'konten' => $request->isi
        ]);
        
    }
}
