<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class LaporanModel extends Model
{
    use HasFactory;

    public function listPekerjaan(){
    	$list = DB::table('order_penugasan')
    		->leftJoin('karyawan', 'karyawan.id', '=', 'order_penugasan.id_karyawan')
    		->leftJoin('order', 'order.id', '=', 'order_penugasan.id_order')
            //->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            //->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            //->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            //->select('member_alamat.*', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            //->where('member_alamat.id_member',$id)
            ->where('order_penugasan.status','<>','delete')
            ->where('order_penugasan.status','=','selesai')
            ->select('order_penugasan.*', 'karyawan.nama as nama_karyawan', 'order.waktu_order', 'order.delivery', 'order.installation')
            ->get();

        return $list;
    }

    public function listPekerjaanByKaryawan($request){
        $list = DB::table('order_penugasan')
            ->leftJoin('karyawan', 'karyawan.id', '=', 'order_penugasan.id_karyawan')
            ->leftJoin('order', 'order.id', '=', 'order_penugasan.id_order')
            ->where('order_penugasan.status','<>','delete')
            ->where('order_penugasan.status','=','selesai')
            ->where('karyawan.nama','like',''.$request->bykaryawan.'%')
            ->select('order_penugasan.*', 'karyawan.nama as nama_karyawan', 'order.waktu_order', 'order.delivery', 'order.installation')
            ->get();

        return $list;
    }

    public function listPekerjaanByTanggal($tgl_mulai,$tgl_selesai){
        $list = DB::table('order_penugasan')
            ->leftJoin('karyawan', 'karyawan.id', '=', 'order_penugasan.id_karyawan')
            ->leftJoin('order', 'order.id', '=', 'order_penugasan.id_order')
            ->where('order_penugasan.status','<>','delete')
            ->where('order_penugasan.status','=','selesai')
            ->whereBetween('order_penugasan.waktu_penugasan', [$tgl_mulai, $tgl_selesai])
            ->select('order_penugasan.*', 'karyawan.nama as nama_karyawan', 'order.waktu_order', 'order.delivery', 'order.installation')
            ->get();

        return $list;
    }



    public function listKategoriProyek(){
            $list= DB::table('proyek_kategori')
                ->where('status', '<>', 'delete')
                ->get();
            return $list;
        
    }

    public function kategoriProyekKeyword($request){
            $list= DB::table('proyek_kategori')
                    ->where('status', '<>', 'delete')
                    ->where('id', '=', $request->kategori_proyek)
                    ->first();
            return $list;
    }

    public function listOrder(){
        $hasil = [];
        $i = 0 ;

        $order = DB::table('order')
            ->leftJoin('order_produk', 'order_produk.id_order', '=', 'order.id')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            /*->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')*/
            ->select('order.id', 'order.waktu_order', 'order.delivery', 'order.nilai_delivery', 'order.nilai_installation' ,'member.nama as nama_member', 'order_produk.qty', 'order_produk.harga_total', 'order_produk.diskon')
            ->get();

        foreach ($order as $item) {

            $status_order = DB::table('order_status')
            ->select('status')
            ->where('id_order', $item->id)
            ->orderBy('waktu_perubahan', 'DESC')
            ->offset(0)
            ->limit(1)
            ->first();

            if(!empty($status_order->status) && $status_order->status == 'selesai'){
                $hasil[$i]['waktu_order'] =  $item->waktu_order;
                $hasil[$i]['id'] =  $item->id;
                $hasil[$i]['nama_member'] =  $item->nama_member;
                $hasil[$i]['qty'] =  $item->qty;
                $hasil[$i]['delivery'] =  $item->delivery;
                $hasil[$i]['status'] =  $status_order->status;
                $hasil[$i]['total_pembelian'] =  ($item->harga_total - $item->diskon) + $item->nilai_delivery + $item->nilai_installation ;
            }else{
                //$hasil[$i]['status'] ="";
            }
            
            $i++;
        }

        return $hasil;
    }


    public function listOrderByTanggal($tgl_mulai, $tgl_selesai){
        $hasil = [];
        $i = 0 ;

        $order = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*','member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->whereBetween('order.waktu_order',[$tgl_mulai, $tgl_selesai])
            ->get();

        foreach ($order as $item) {
            $hasil[$i]['waktu_order'] =  $item->waktu_order;
            $hasil[$i]['id'] =  $item->id;
            $hasil[$i]['nama_member'] =  $item->nama_member;
            $hasil[$i]['delivery'] =  $item->delivery;
            
            $status_order = DB::table('order_status')
            ->select('status as order_status')
            ->where('id_order', $item->id)
            ->orderBy('waktu_perubahan', 'DESC')
            ->offset(0)
            ->limit(1)
            ->first();

            if(!empty($status_order->order_status)){
                $hasil[$i]['status'] =  $status_order->order_status;
            }else{
                $hasil[$i]['status'] ="-";
            }
            
            $i++;
        }

        return $hasil;
    }

    
    public function listOrderByJumlah($request){
        $hasil = [];
        $i = 0 ;

        $order = DB::table('order')
            ->leftJoin('order_produk', 'order_produk.id_order', '=', 'order.id')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->select('order.id', 'order.waktu_order', 'order.delivery', 'order.nilai_delivery', 'order.nilai_installation' ,'member.nama as nama_member', 'order_produk.qty', 'order_produk.harga_total', 'order_produk.diskon')
            ->where('order_produk.qty','like',$request->byjumlah.'%')
            ->get();

        foreach ($order as $item) {

            $status_order = DB::table('order_status')
            ->select('status as order_status')
            ->where('id_order', $item->id)
            ->orderBy('waktu_perubahan', 'DESC')
            ->offset(0)
            ->limit(1)
            ->first();

            if(!empty($status_order->order_status && $status_order->order_status == 'selesai')){
                $hasil[$i]['waktu_order'] =  $item->waktu_order;
                $hasil[$i]['id'] =  $item->id;
                $hasil[$i]['nama_member'] =  $item->nama_member;
                $hasil[$i]['qty'] =  $item->qty;
                $hasil[$i]['delivery'] =  $item->delivery;
                $hasil[$i]['status'] =  $status_order->order_status;
                $hasil[$i]['total_pembelian'] =  ($item->harga_total - $item->diskon) + $item->nilai_delivery + $item->nilai_installation ;
            }else{
                //$hasil[$i]['status'] ="-";
            }
            
            $i++;
        }

        return $hasil;
    }

    public function listOrderByKategori($request){
        $hasil = [];
        $i = 0 ;

        $order = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*','member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->where('order.id_proyek',$request->kategori_proyek)
            ->get();

        foreach ($order as $item) {

            
            $status_order = DB::table('order_status')
            ->select('status as order_status')
            ->where('id_order', $item->id)
            ->orderBy('waktu_perubahan', 'DESC')
            ->offset(0)
            ->limit(1)
            ->first();

            if(!empty($status_order->order_status) && $status_order->order_status == 'selesai'){
                $hasil[$i]['waktu_order'] =  $item->waktu_order;
                $hasil[$i]['id'] =  $item->id;
                $hasil[$i]['nama_member'] =  $item->nama_member;
                $hasil[$i]['delivery'] =  $item->delivery;
                $hasil[$i]['id_proyek'] =  $item->id_proyek;
                $hasil[$i]['status'] =  $status_order->order_status;
            }else{
                //$hasil[$i]['status'] ="-";
            }
            
            $i++;
        }

        return $hasil;
    }
    
}
