<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatabl;
use Illuminate\Support\Facades\Auth;
use Helper as helper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Collection;

class MemberModel extends Model
{
    use HasFactory;

    public function listMember(){
    	$list= DB::table('member')
            ->where('status', '<>', 'delete')
            ->get();
    	return $list;
    }

    public function tambahMember($request){
        
      	DB::table('member')->insert([
        'nama' => $request->nama,
        'no_hp' => $request->nohp,
        'email' => $request->email,
        'password' => Hash::make('01X'.$request->password.'@#'),
        'status' => $request->status
        ]);
    }

    public function editMember($id){
        
        //first() aka singleresult (cuma 1 data)
        //get() aka listresults (data yg diambil banyak)

    	$list = DB::table('member')->where('id',$id)->first();
    	return $list;
    }

    public function prosesUpdate($request){
        $password = $request->password;
        if(!empty($password)){

        	$list = DB::table('member')->where('id',$request->id)->update([
    		'nama' => $request->nama,
    		'no_hp' => $request->nohp,
            'email' => $request->email,
            'password' => Hash::make('01X'.$request->password.'@#'),
            'status' => $request->status
    		]);

        }else{

            $list = DB::table('member')->where('id',$request->id)->update([
            'nama' => $request->nama,
            'no_hp' => $request->nohp,
            'email' => $request->email,
            'status' => $request->status
            ]);
        }
    }

    public function hapusMember($id){
    	$list = DB::table('member')->where('id',$id)->delete();
    	return $list;
    }


    public function detilMember($id){
        $list = DB::table('member')->where('id',$id)->first();
        return $list;
    }

    public function alamatMember($id){
        $list = DB::table('member_alamat')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('member_alamat.*', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->where('member_alamat.id_member',$id)
            ->get();

        return $list;
    }


    public function listOrderDetilMember($id_member){
        $hasil = array();
        $i = 0 ;
        $orderan = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*', 'member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->where('order.id_member',$id_member)
            ->get();


        foreach ($orderan as $item) {
          $hasil['order'][$i]['id_order'] =  $item->id;
          $hasil['order'][$i]['waktu_order'] =  $item->waktu_order;
          $hasil['order'][$i]['alamat'] =  $item->alamat;
          $hasil['order'][$i]['nama_kecamatan'] =  $item->nama_kecamatan;
          $hasil['order'][$i]['nama_kabupaten'] =  $item->nama_kabupaten;
          $hasil['order'][$i]['nama_provinsi'] =  $item->nama_provinsi;
          $hasil['order'][$i]['delivery'] =  $item->delivery;
          $hasil['order'][$i]['request_ready'] =  $item->request_ready;


           $status_order = DB::table('order_status')
            ->select('status as order_status')
            ->where('id_order', $item->id)
            ->orderBy('waktu_perubahan', 'DESC')
            ->offset(0)
            ->limit(1)
            ->first();

            if(!empty($status_order->order_status)){
                 $hasil['order'][$i]['status'] = $status_order->order_status;
            }else{
                $hasil['order'][$i]['status'] ="-";
            }

          $produk = DB::table('order_produk')
            ->leftJoin('produk', 'produk.id', '=', 'order_produk.id_produk')
            ->select('order_produk.*', 'produk.nama as nama_produk')
            ->where('order_produk.id_order',$item->id)
            ->get();

          $j = 0;
          foreach ($produk as $row) {
             $hasil['order'][$i]['produk'][$j]['nomor_order'] = $row->id_order;
             $hasil['order'][$i]['produk'][$j]['nama_produk'] = $row->nama_produk;
             $hasil['order'][$i]['produk'][$j]['qty'] = $row->qty; 
             $hasil['order'][$i]['produk'][$j]['harga_total'] = $row->harga_total;
             $hasil['order'][$i]['produk'][$j]['diskon'] = $row->diskon;
             $hasil['order'][$i]['produk'][$j]['satuan_diskon'] = $row->satuan_diskon;
          $j++;
          }

          $i++;
        }
        return $hasil;
    }

    /*
    public function listOrder(){
        $list = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*', 'member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->get();

        return $list;
    }

    public function order($id_order){
        $list = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*', 'member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->where('order.id',$id_order)
            ->first();

        return $list;
    }

    public function produkOrder($id_order){
        $list = DB::table('order_produk')
            ->leftJoin('produk', 'produk.id', '=', 'order_produk.id_produk')
            ->select('order_produk.*', 'produk.nama as nama_produk')
            ->where('order_produk.id_order',$id_order)
            ->get();

        return $list;
    } */

}
