<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class OrderModel extends Model
{
    use HasFactory;

    public function listOrder(){
        $hasil = [];
        $i = 0 ;

        $order = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->select('order.*','member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
            ->get();

        foreach ($order as $item) {
            $hasil[$i]['waktu_order'] =  $item->waktu_order;
            $hasil[$i]['id'] =  $item->id;
            $hasil[$i]['nama_member'] =  $item->nama_member;
            $hasil[$i]['delivery'] =  $item->delivery;
            
            $status_order = DB::table('order_status')
            ->select('status as order_status')
            ->where('id_order', $item->id)
            ->orderBy('waktu_perubahan', 'DESC')
            ->offset(0)
            ->limit(1)
            ->first();

            if(!empty($status_order->order_status)){
                $hasil[$i]['status'] =  $status_order->order_status;
            }else{
                $hasil[$i]['status'] ="-";
            }
            
            $i++;
        }

        return $hasil;
    }

    public function order($id_order){
        $list = DB::table('order')
            ->leftJoin('member', 'member.id', '=', 'order.id_member')
            ->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
            ->leftJoin('proyek_kategori','proyek_kategori.id', '=', 'order.id_proyek' )
            ->leftJoin('biaya_installation', 'biaya_installation.id', '=', 'order.id_installation')
            ->leftJoin('biaya_delivery', 'biaya_delivery.id', '=', 'order.id_delivery')
            ->leftJoin('kendaraan', 'kendaraan.id', '=', 'biaya_delivery.id_kendaraan')
            ->select('order.*', 'member.nama as nama_member', 'member_alamat.nama as an_nama', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi', 'proyek_kategori.nama as nama_proyek', 'biaya_installation.nama as nama_instalasi', 'biaya_installation.harga as harga_instalasi', 'kendaraan.nama as nama_kendaraan')
            ->where('order.id',$id_order)
            ->first();

        return $list;
    }

    public function produkOrder($id_order){
        $list = DB::table('order_produk')
            ->leftJoin('produk', 'produk.id', '=', 'order_produk.id_produk')
            ->select('order_produk.*', 'produk.nama as nama_produk')
            ->where('order_produk.id_order',$id_order)
            ->get();

        return $list;
    }

    public function orderStatus($id_order){
        $list = DB::table('order')
            ->leftJoin('order_status', 'order_status.id_order', '=', 'order.id')
            ->leftJoin('karyawan', 'karyawan.id', '=', 'order_status.id_admin')
            ->select('order_status.id_order', 'order_status.waktu_perubahan', 'order_status.status', 'karyawan.nama as nama_karyawan' )
            ->where('order_status.id_order',$id_order)
            ->get();

        return $list;
    }

    public function orderPembayaran($id_order){
        $list = DB::table('order_konfirmasi')
            ->select('id_order', 'waktu_konfirmasi', 'nominal_transfer', 'bank', 'atas_nama', 'bukti_transfer', 'status'  )
            ->where('id_order',$id_order)
            ->first();

        return $list;
    }

    public function listKaryawan(){
        $list = DB::table('karyawan')
            ->leftJoin('karyawan_divisi', 'karyawan_divisi.id', '=', 'karyawan.divisi')
            ->where('karyawan.status','<>','delete')
            ->select('karyawan.*', 'karyawan_divisi.nama as nama_divisi', 'karyawan_divisi.id as id_divisi' )
            ->get();

        return $list;
    }
    

    //menu konfirmasi pembayaran
    public function konfirmasiPembayaran(){

        $list = DB::table('order_konfirmasi')
        	->leftJoin('order', 'order.id', '=', 'order_konfirmasi.id_order')
        	->leftJoin('member', 'member.id', '=', 'order.id_member')
        	->leftJoin('member_alamat', 'member_alamat.id', '=', 'order.alamat_kirim')
            ->leftJoin('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
            ->leftJoin('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
            ->leftJoin('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
        	->select('order_konfirmasi.*','order.waktu_order', 'order.delivery', 'member.nama as nama_member', 'member_alamat.alamat', 'kecamatan.nama as nama_kecamatan', 'kabupaten.nama as nama_kabupaten', 'provinsi.nama as nama_provinsi')
        	->get();
        return $list;
    }

    public function prosesUbahKonfirmasi($request){

        $list = DB::table('order_konfirmasi')->where('id',$request->id_konfirmasi)->update([
        'status' => $request->status
		]); 

        if($request->status == 'Lunas' && !empty($request->id_order_hidden)){
            
            $id_admin = session()->get('id');

            DB::table('order_status')->insert([
            'id_order' => $request->id_order_hidden,
            'waktu_perubahan' => date('Y-m-d H:i:s'),
            'status' => 'diproses',
            'id_admin' => $id_admin
            ]);
        }
    }


}
