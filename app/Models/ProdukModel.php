<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class ProdukModel extends Model
{
    use HasFactory;

    //fungsi untuk short url bitly
    public function make_bitly_url($url) {
        $bitly = 'http://api.bit.ly/shorten?version=2.0.1&longUrl='.urlencode($url).'&login=eliantoro&apiKey=R_6f3c3a049e8942c0abafea40beb6526b&format=json';
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$bitly);
        $result=curl_exec($ch);
        curl_close($ch);
        
        $json = @json_decode($result,true);
        return $json['results'][$url]['shortUrl'];
    }

    public function listProduk(){
    	$list= DB::table('produk')->get();
    	return $list;
    }



    public function editProduk($id){
        
        //first() aka singleresult (cuma 1 data)
        //get() aka listresults (data yg diambil banyak)

    	$list = DB::table('produk')->where('id',$id)->first();
    	return $list;
    }

    public function editProdukHarga($id){
        $list = DB::table('produk_harga')->where('id_produk',$id)->get();
        return $list;
    }

    public function editProdukGaleri($id){
        $list = DB::table('produk_galeri')->where('id_produk',$id)->get();
        return $list;
    }

	public function prosesUpdate($request){
        //$request->id adalah id produk

    	$list = DB::table('produk')->where('id',$request->id)->update([
		'nama' => $request->nama,
		'deskripsi' => $request->deskripsi,
		'status' => $request->status
		]);

        //harga
        DB::table('produk_harga')->where('id_produk',$request->id)->delete();
        foreach($request->harga as $index => $value) {
           $harga = $value;
            if(!empty($harga)){
            DB::table('produk_harga')->insert([
                        'id_produk' => $request->id,
                        'qty' => $request->qty[$index],
                        'harga' => $harga,
                        'status' => $request->status_harga[$index]
                     ]);

                
           }

        }
        if(!empty($request->harga00)){
            DB::table('produk_harga')->insert([
                        'id_produk' => $request->id,
                        'qty' => $request->qty00,
                        'harga' => $request->harga00,
                        'status' => $request->status_harga00
            ]);
        }

        //galeri image
        $file = $request->file('namafile');
        if(!empty($file) && $request->kategori == 'image' && !empty($request->judul)){
            $nama_file = date('Ymd').time().".".$file->getClientOriginalExtension();

           DB::table('produk_galeri')->where('id',$request->id)->delete();

           DB::table('produk_galeri')->insert([
                        'id_produk' => $request->id,
                        'title' => $request->judul,
                        'kategori' => $request->kategori,
                        'nama_file' => $nama_file,
                        'thumbnail' => $nama_file,
                        'status' => $request->status_galeri
                     ]);

            $tujuan_upload = 'storage';
            $file->move($tujuan_upload,$nama_file);
        }else if(empty($file) && !empty($request->namafile_hidden && $request->kategori == 'image') ){
           //tidak menambah data
            
            
        }

        //galeri video
        if($request->url <> $request->url_hidden){
        $videoUrl = $request->url; 
        parse_str( parse_url( $videoUrl, PHP_URL_QUERY ), $my_array_of_vars );
        $id_video = $my_array_of_vars['v']; //gets video ID

        $thumbnail_ytb = "http://img.youtube.com/vi/".$id_video."/maxresdefault.jpg";

        DB::table('produk_galeri')->insert([
                        'id_produk' => $request->id,
                        'title' => $request->judul_video,
                        'kategori' => $request->kategori_galeri_video,
                        'nama_file' => $request->url,
                        'thumbnail' => $thumbnail_ytb,
                        'status' => $request->status_galeri_video
                     ]);
        }

    }

    public function hapusProduk($id){
    	$list = DB::table('produk')->where('id',$id)->delete();
    	return $list;
    }

    public function tambahProduk($request){

        $judul_kecil = strtolower($request->nama);
        $meta_url = str_replace(' ', '-', $judul_kecil);

        $url_domain = "https://klop.ptabcd.xyz/".$meta_url; //url("/produk/detail/".$meta_url);
        $buat_short_url = $this->make_bitly_url($url_domain);

        $cek_meta_url= DB::table('produk')
            ->where('meta_url',$meta_url)
            ->first();

        if(!is_null($cek_meta_url)){

        //insertGetId => untuk mengambil id terakhir di inputkan aka last_insert_id()
        $id = DB::table('produk')->insertGetId([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'status' => $request->status_produk
        ]);

        $new_meta_url = $meta_url.'-'.$id;
        $url_domain_new = "https://klop.ptabcd.xyz/".$new_meta_url; //url("/produk/detail/".$meta_url);
        $buat_short_url_new = $this->make_bitly_url($url_domain_new);

            DB::table('produk')->where('id',$id)->update([
            'meta_url' => $new_meta_url,
            'short_url' => $buat_short_url_new
            ]);

        }else{
            //insertGetId => untuk mengambil id terakhir di inputkan aka last_insert_id()
            $id = DB::table('produk')->insertGetId([
                'nama' => $request->nama,
                'deskripsi' => $request->deskripsi,
                'status' => $request->status_produk,
                'meta_url' => $meta_url,
                'short_url' => $buat_short_url
            ]);
        }
       
        //harga
        foreach ($request->qty as $index => $value) {
            $qty = $value;
            if(!empty($qty)){
            DB::table('produk_harga')->insert([
                        'id_produk' => $id,
                        'qty' => $qty,
                        'harga' => $request->harga[$index],
                        'status' => $request->status_harga[$index]
                     ]);
           }
        } 

        //galeri image
        $file = $request->file('namafile');
        //$filename = $file->getClientOriginalName();
        //$filepisah = explode('.',$filename);
        //$file_exstensi = $filepisah[1];

        $nama_file = date('Ymd').time().".".$file->getClientOriginalExtension();;

        //$nama_file = time()."_".$file->getClientOriginalName();

         DB::table('produk_galeri')->insert([
                        'id_produk' => $id,
                        'title' => $request->judul_foto,
                        'kategori' => 'image',
                        'nama_file' => $nama_file,
                        'thumbnail' => $nama_file,
                        'status' => $request->status_foto
                     ]);
           
        $tujuan_upload = 'storage/galeri/';
        $file->move($tujuan_upload,$nama_file);

        
        //galeri video
        foreach ($request->url_video as $key => $value) {
           $videoUrl = $value;
        

            parse_str( parse_url( $videoUrl, PHP_URL_QUERY ), $my_array_of_vars );
            $id_video = $my_array_of_vars['v']; //gets video ID

            $thumbnail_ytb = "http://img.youtube.com/vi/".$id_video."/maxresdefault.jpg";

            DB::table('produk_galeri')->insert([
                        'id_produk' => $id,
                        'title' => $request->judul_video[$key],
                        'kategori' => 'video',
                        'nama_file' => $videoUrl,
                        'thumbnail' => $thumbnail_ytb,
                        'status' => $request->status_video[$key]
                     ]); 
        }
        
       


       /*
       DB::table('produk_galeri')->insert([
        'id_produk' => $id,
        'kategori' => $request->kategori,
        'title' => $request->judul,
        'nama_file' => $request->namafile,
        'status' => $request->status
        ]);
       */
    }


    public function detilProduk($id_produk){
        $list= DB::table('produk')
                ->where('id',$id_produk)
                ->first();
        return $list;
    }

    public function detilProdukHarga($id_produk){
        $list= DB::table('produk_harga')
                ->where('id_produk',$id_produk)
                ->get();
        return $list;
    }

    public function detilProdukGaleri($id_produk){
        $list= DB::table('produk_galeri')
                ->where('id_produk',$id_produk)
                ->get();
        return $list;
    }
    
    
}
