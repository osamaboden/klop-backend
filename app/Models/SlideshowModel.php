<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class SlideshowModel extends Model
{
    use HasFactory;

    public function listSlideshow(){
    	$list= DB::table('slide_show')
    			->select('id','gambar', 'status', 'kategori')
    			->get();
    	return $list;
    }

    public function tambah($request){
        //image
        $file = $request->file('gambar');
        $nama_file = date('Ymd').time().".".$file->getClientOriginalExtension();

         DB::table('slide_show')->insert([
        'gambar' => $nama_file,
        'status' => $request->status,
        'kategori' => $request->kategori
        ]);
           
        $tujuan_upload = 'storage/corousel/';
        $file->move($tujuan_upload,$nama_file);
	
    }

    public function prosesUpdate($request){
    	$file = $request->file('gambar');
    	$filehide = $request->namafile_hidden;
        

        if(!empty($file)){
        	$nama_file = date('Ymd').time().".".$file->getClientOriginalExtension();
	    	$list = DB::table('slide_show')->where('id',$request->id)->update([
			'gambar' => $nama_file,
	        'status' => $request->status,
	        'kategori' => $request->kategori
			]);

			$tujuan_upload = 'storage/corousel/';
	        $file->move($tujuan_upload,$nama_file);

	        //$filehide->delete($tujuan_upload,$filehide);
	        //$hapusfilename = $tujuan_upload.$filehide;
	        File::delete('storage/corousel/'.$filehide);

        }else{

        	$list = DB::table('slide_show')->where('id',$request->id)->update([
	        'status' => $request->status,
	        'kategori' => $request->kategori
			]);
        }
    }

    public function hapus($id){

    	$list = DB::table('slide_show')->where('id',$id)->update([
        'status' => 'delete'
		]);	
    }
}
