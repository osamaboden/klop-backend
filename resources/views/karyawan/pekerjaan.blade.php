  <?PHP
  $menu = "Pekerjaan";
  $page = "Daftar Pekerjaan";
  ?>
  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>&nbsp;</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Daftar Pekerjaan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pekerjaan</h3>
                <div class="text-right">
                  <!--
                  <a class="btn btn-info btn-sm" href="/member/tambah">
                      <i class="fas fa-plus"></i> Tambah
                  </a> -->
                  <button id="btnTambah" type="button" class="btn btn-info btn-sm" data-toggle="modal">
                    <i class="fas fa-plus"></i> Tambah
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              
              <div class="card-body">
                               
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    
                    <th>Id Order</th>
                    <th>Waktu Order</th>
                    <th>Status</th>
                    <th>Nama Karyawan</th>
                    <th>Waktu Pengerjaan</th>
                    <!--<th>Pengiriman</th>
                    <th>Pemasangan</th> -->
                    <th>Pekerjaan</th>
                   
                    <th class="aksi">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($listpekerjaan as $row)
                  <tr data-id="{{ $row->id }}" data-karyawan="{{ $row->id_karyawan }}" data-id_order="{{ $row->id_order }}" data-kategori="{{ $row->kategori }}" data-waktu_penugasan="<?PHP echo date('d-m-Y H:i:s',strtotime($row->waktu_penugasan)); ?>" data-status="{{ $row->status }}">
                    <td>{{ $row->id_order }}</td>
                    <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_order)); ?></td>
                    <td>{{ $row->status }}</td>
                    <td>{{ $row->nama_karyawan }}</td>
                    <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_penugasan)); ?></td>
                    <td>{{ $row->kategori }}</td> 
                    <!--<td>{{ $row->delivery }}</td>
                    <td>{{ $row->installation }}</td> -->
                    <td class="project-actions text-right">
                        <!-- <a class="btn btn-primary btn-sm" href="order/detilorder/{{ $row->id_order }}" title="Detil">
                          <i class="fas fa-folder"></i>
                        </a> -->

                        <a class="btn btn-primary btn-sm" href="/daftarpekerjaan/detil-pekerjaan/{{ $row->id }}" title="Detil">
                          <i class="fas fa-folder"></i>
                        </a>
                        
                        <a class="btn btn-info btn-sm" data-target="#modal-edit" >
                          <i class="fas fa-pencil-alt"></i>
                        </a>

                        <a class="btn btn-danger btn-sm" href="/daftarpekerjaan/hapus-pekerjaan/{{ $row->id }}">
                          <i class="fas fa-trash"></i>
                        </a>
                        <!--
                        <a class="btn btn-info btn-sm" href="/member/edit/{{ $row->id }}">
                          <i class="fas fa-pencil-alt"></i> Ubah
                        </a>--> 
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                  
                </table>

              </div>
              <!-- /.card-body -->

            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->

      <!-- modal -->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <form action="/daftarpekerjaan/simpan-pekerjaan" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}
              <div class="form-group">
                  <label>Id Order </label>
                  <select name="id_order" class="form-control">
                  <?PHP
                  foreach ($listorder as $item) {
                  ?>
                  <option value="<?PHP echo $item->id_order; ?>"><?PHP echo $item->id_order; ?></option>
                  <?PHP
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="baru">Baru</option>
                  <option value="dikerjakan">Dikerjakan</option>
                  <option value="selesai">Selesai</option>
                </select>
              </div>
              <div class="form-group">
                <label>Nama Karyawan</label>
                <select name="karyawan" class="form-control">
                  <?PHP
                  foreach ($listkaryawan as $item) {
                  ?>
                  <option value="<?PHP echo $item->id; ?>"><?PHP echo $item->nama; ?></option>
                  <?PHP
                  }
                  ?>
                </select>
              </div>
              <!-- Date and time -->
              <div class="form-group">
                <label>Waktu Pengerjaan</label>
                  <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input" name="waktu_penugasan" data-target="#reservationdatetime"/>
                    <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <label>Pekerjaan</label>
                <select name="kategori" class="form-control">
                  <option value="delivery">Delivery</option>
                  <option value="installation">Instalation</option>
                </select>
              </div>
              <!-- /.form group -->

              <!--<div class="form-group">
                <label>Divisi</label>
                <select name = "divisi" class="form-control">
                  <option value="0">Divisi</option>
                  @foreach($listdivisi as $baris)
                  <option value="{{$baris->id}}">{{$baris->nama}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Hak Akses</label>
                <select name="hak_akses" class="form-control">
                  <option value="0">Hak Akses</option>
                  <option value="admin">Admin</option>
                  <option value="karyawan">Karyawan</option>
                </select>
              </div> -->
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- modal edit -->
      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <form action="/daftarpekerjaan/update-pekerjaan" method="post" >
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}   
              <input type="hidden" name="id">
              <div class="form-group">
                  <label>Id Order </label>
                  <input type="hidden" name="id_order_hidden">
                  <select name="id_order" class="form-control">
                  <?PHP
                  foreach ($listorder as $item) {
                  ?>
                  <option value="<?PHP echo $item->id_order; ?>"><?PHP echo $item->id_order; ?></option>
                  <?PHP
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="baru">Baru</option>
                  <option value="dikerjakan">Dikerjakan</option>
                  <option value="selesai">Selesai</option>
                </select>
              </div>
              <div class="form-group">
                <label>Nama Karyawan</label>
                <select name="karyawan" class="form-control">
                  <?PHP
                  foreach ($listkaryawan as $item) {
                  ?>
                  <option value="<?PHP echo $item->id; ?>"><?PHP echo $item->nama; ?></option>
                  <?PHP
                  }
                  ?>
                </select>
              </div>
              <!-- Date and time -->
              <div class="form-group">
                <label>Waktu Pengerjaan</label>
                  <div class="input-group date" id="tgldanwaktu" data-target-input="nearest">
                    <input type="text" name="waktu_penugasan_edit" class="form-control datetimepicker-input" data-target="#tgldanwaktu"/>
                    <div class="input-group-append" data-target="#tgldanwaktu" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <label>Pekerjaan</label>
                <select name="kategori" class="form-control">
                  <option value="delivery">Delivery</option>
                  <option value="installation">Instalation</option>
                </select>
              </div>
              <!-- /.form group -->
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  
  <!-- Bootstrap4 Duallistbox -->
  <script src="{{ asset('assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
  
  
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- Select2 -->
  <script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }} "></script>
  <!-- InputMask -->
  <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/inputmask/jquery.inputmask.min.js ') }}"></script>
  <!-- date-range-picker -->
  <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }} "></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#example1 tbody').on('click', 'tr', function(){
        var id = $(this).attr('data-id');
        var karyawan = $(this).attr('data-karyawan');
        var id_order = $(this).attr('data-id_order');
        var kategori = $(this).attr('data-kategori');
        var waktu_penugasan = $(this).attr('data-waktu_penugasan');
        var status = $(this).attr('data-status');

            $('input[name="id"]').val(id);
            $('select[name="karyawan"]').val(karyawan);
            $('select[name="id_order"]').val(id_order);
            $('input[name="id_order_hidden"]').val(id_order);
            $('select[name="kategori"]').val(kategori);
            $('input[name="waktu_penugasan_edit"]').val(waktu_penugasan);
            $('select[name="status"]').val(status);
            $('#modal-edit').modal('show');
          
      }); 

      /*
      $('#btnEdit').on('click', function(){
          var karyawan = $(this).attr('data-karyawan');

          $('input[name="id"]').val('');
          $('select[name="karyawan"]').val('anco');
          $('select[name="id_order"]').val('');
          $('select[name="kategori"]').val('');
          $('input[name="waktu_penugasan"]').val('');
          $('select[name="status"]').val('0');
          $('#modal-edit').modal('show');
      });*/

      $('#btnTambah').on('click', function(){
          $('input[name="id"]').val('');
          $('select[name="status"]').val('baru');
          $('#modal-default').modal('show');
      });

      //Date and time picker
      $('#reservationdatetime').datetimepicker({  
        format: 'DD-MM-YYYY HH:mm:ss',
        icons: { time: 'far fa-clock' }
       });

      $('#tgldanwaktu').datetimepicker({  
        format: 'DD-MM-YYYY HH:mm:ss',
        icons: { time: 'far fa-clock' }
       });
  
    });
  </script>
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });

        

    });
  </script>

@include('layouts/footer')