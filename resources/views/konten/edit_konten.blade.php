  <?PHP
  $menu = "Konten";
  $page = "Edit Konten";
  ?>
  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>&nbsp;</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Home</a></li>
              <li class="breadcrumb-item active"><?PHP echo $page; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12">
            <div class="card card-primary card-outline">
              <form action="/konten/simpan" method="post">
                {{ csrf_field() }}
              <div class="card-header">
                <h3 class="card-title"><?PHP echo $page; ?></h3>
              </div>
              <!-- /.card-header -->
              
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" name="id" class="form-control" value="{{$editkonten->id}}">
                  <input type="text" name="judul" class="form-control" placeholder="judul" value="{{$editkonten->title}}">
                </div>
                <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" style="height: 300px" name="isi">
                      {{$editkonten->konten}}
                    </textarea>
                </div>
               
              </div>
              <!-- /.card-body -->
              
              <div class="card-footer">
                <div class="float-right">
                  <button type="submit" class="btn btn-primary"></i> Simpan</button>
                </div>
                
                <a class="btn btn-default" href="/konten">
                  Kembali
                </a>
              </div>
              <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- Summernote -->
  <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#example1XXXXXXX tbody').on('click', 'tr', function(){
        var id = $(this).attr('data-id');
        var nama = $(this).attr('data-nama');
        var no_hp = $(this).attr('data-nohp');
        var email = $(this).attr('data-email');
        var status = $(this).attr('data-status');

            $('input[name="id"]').val(id);
            $('input[name="nama"]').val(nama);
            $('input[name="nohp"]').val(no_hp);
            $('input[name="email"]').val(email);
            $('select[name="status"]').val(status);
            $('#modal-edit').modal('show');
          
      });

      $('#btnTambah').on('click', function(){
          $('input[name="id"]').val('');
          $('input[name="nama"]').val('');
          $('input[name="nohp"]').val('');
          $('input[name="email"]').val('');
          $('select[name="status"]').val('aktif');
          $('#modal-default').modal('show');
      });
  
    });

    function ubahData(id, nama, status){
        var id = id ;
        var nama = nama;
        var status = status;

            $('input[name="id"]').val(id);
            $('input[name="nama"]').val(nama);
            $('select[name="status"]').val(status);
            $('#modal-edit').modal('show');
    }

    function handleDelete(id){
        var link = document.getElementById('deleteLink');
        link.href = "/karyawandivisi/hapus/" + id

        $('#modal-edit').modal('hide');
        $('#modal-konformasi-hapus').modal('show');
    }

  </script>
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });

    $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
  </script>

@include('layouts/footer')