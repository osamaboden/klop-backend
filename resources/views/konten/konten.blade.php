  <?PHP
  $menu = "Konten";
  $page = "Daftar Konten";
  ?>
  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>&nbsp;</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Home</a></li>
              <li class="breadcrumb-item active"><?PHP echo $page; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?PHP echo $page; ?></h3>
                <div class="text-right">
                  <!--
                  <a class="btn btn-info btn-sm" href="/member/tambah">
                      <i class="fas fa-plus"></i> Tambah
                  </a> 
                  <button id="btnTambah" type="button" class="btn btn-info btn-sm" data-toggle="modal">
                    <i class="fas fa-plus"></i> Tambah
                  </button> -->
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nama</th>
                    <th class="aksi">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($listkonten as $row)
                  <tr>
                    <td>{{ $row->title }}</td>
                    <td class="project-actions text-right">
                        <!--<a class="waiki btn btn-primary btn-sm" href="/member/detil/{{ $row->id }}" id="waiki" title="Detil">
                          <i class="fas fa-folder"></i>
                        
                        </a> -->
                        
                        <a class="btn btn-info btn-sm" href="/konten/edit/{{ $row->id }}">
                          <i class="fas fa-pencil-alt"></i>
                        </a> 
                        <!--
                        <a class="btn btn-info btn-sm" data-target="#modal-edit" title="Ubah" onclick="ubahData('{{ $row->id }}','{{ $row->title }}','{{ $row->konten }}')">
                          <i class="fas fa-pencil-alt"></i> 
                        </a>
                        <a class="btn btn-danger btn-sm" title="hapus" onclick="handleDelete('{{ $row->id }}')">
                          <i class="fas fa-trash"></i>
                        </a> -->
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                  
                </table>

              </div>
              <!-- /.card-body -->

            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->

      <!-- modal -->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <form action="/karyawandivisi/simpan" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}            
              <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" required="required" name="nama" placeholder="Enter ...">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="aktif">Aktif</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- modal edit -->
      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <form action="/karyawandivisi/update" method="post" >
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}   
              <input type="hidden" name="id">          
              <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" required="required" name="nama" placeholder="Enter ...">
              </div>
              
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="aktif">Aktif</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- modal konfirmasi hapus -->
      <div class="modal fade" id="modal-konformasi-hapus">
        <div class="modal-dialog">
         
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi hapus</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            Apakah yakin ingin menghapus data ini?
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <a type="submit" id="deleteLink" class="btn btn-primary">Ya</a>
            </div>
          </div>
          <!-- /.modal-content -->
         
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#example1XXXXXXX tbody').on('click', 'tr', function(){
        var id = $(this).attr('data-id');
        var nama = $(this).attr('data-nama');
        var no_hp = $(this).attr('data-nohp');
        var email = $(this).attr('data-email');
        var status = $(this).attr('data-status');

            $('input[name="id"]').val(id);
            $('input[name="nama"]').val(nama);
            $('input[name="nohp"]').val(no_hp);
            $('input[name="email"]').val(email);
            $('select[name="status"]').val(status);
            $('#modal-edit').modal('show');
          
      });

      $('#btnTambah').on('click', function(){
          $('input[name="id"]').val('');
          $('input[name="nama"]').val('');
          $('input[name="nohp"]').val('');
          $('input[name="email"]').val('');
          $('select[name="status"]').val('aktif');
          $('#modal-default').modal('show');
      });
  
    });

    function ubahData(id, nama, status){
        var id = id ;
        var nama = nama;
        var status = status;

            $('input[name="id"]').val(id);
            $('input[name="nama"]').val(nama);
            $('select[name="status"]').val(status);
            $('#modal-edit').modal('show');
    }

    function handleDelete(id){
        var link = document.getElementById('deleteLink');
        link.href = "/karyawandivisi/hapus/" + id

        $('#modal-edit').modal('hide');
        $('#modal-konformasi-hapus').modal('show');
    }

  </script>
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>

@include('layouts/footer')