 <?PHP
  $menu = "Home";
  $page = "Dashboard";
  ?>

  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        
        <!-- ======================= -->
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-order" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Order Baru</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-menunggu" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Menunggu Konfirmasi</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-pekerjaan" role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Pekerjaan</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-four-order" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                     <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Tanggal Order</th>
                        <th>Id Order</th>
                        <th>Nama</th>
                        <th>Delivery</th>
                        <!-- <th>Status</th>  -->
                        <th class="aksi">Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?PHP foreach($listorder as $row){ 
                        if(!empty($row['status']) && $row['status'] == 'baru' ){
                      ?>
                      <tr>
                        <td><?PHP echo date("d M Y H:i:s",strtotime($row['waktu_order'])); ?></td>
                        <td><?PHP echo $row['id']; ?></td>
                        <td><?PHP echo $row['nama_member']; ?></td>
                        <td><?PHP if($row['delivery'] == 'ya'){ echo"Diantar"; }else{ echo "Diambil";} ?></td>
                        <!--<td><?PHP echo $row['status']; ?></td>-->
                      
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm" href="/order/detilorder/<?PHP echo $row['id']; ?>" title="Detil">
                              <i class="fas fa-folder"></i>
                            </a>
                        </td>
                      </tr>
                      <?PHP
                          }
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-menunggu" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                     <table id="konfirmasi" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Tanggal Order</th>
                        <th>Id Order</th>
                        <th>Nama</th>
                        <th>Delivery</th>
                        <!-- <th>Status</th> -->
                        <th class="aksi">Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?PHP foreach($listorder as $row){ 
                        if(!empty($row['status']  && $row['status'] == 'menunggu konfirmasi')){
                      ?>
                      <tr>
                        <td><?PHP echo date("d M Y H:i:s",strtotime($row['waktu_order'])); ?></td>
                        <td><?PHP echo $row['id']; ?></td>
                        <td><?PHP echo $row['nama_member']; ?></td>
                        <td><?PHP if($row['delivery'] == 'ya'){ echo"Diantar"; }else{ echo "Diambil";} ?></td>
                        <!--<td><?PHP echo $row['status']; ?></td> -->
                      
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm" href="/order" title="Detil">
                              <i class="fas fa-folder"></i>
                            </a>
                        </td>
                      </tr>
                      <?PHP
                          }
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-pekerjaan" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
                     <table id="pekerjaan" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        
                        <th>Id Order</th>
                        <th>Waktu Order</th>
                        <th>Nama Karyawan</th>
                        <th>Waktu Penugasan</th>
                        <!-- <th>Kategori</th> -->
                       
                        <th class="aksi">Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($listpekerjaan as $row)
                      <tr data-id="{{ $row->id }}" data-karyawan="{{ $row->id_karyawan }}" data-id_order="{{ $row->id_order }}" data-kategori="{{ $row->kategori }}" data-waktu_penugasan="<?PHP echo date('d-m-Y H:i:s',strtotime($row->waktu_penugasan)); ?>" data-status="{{ $row->status }}">
                        <td>{{ $row->id_order }}</td>
                        <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_order)); ?></td>
                        <td>{{ $row->nama_karyawan }}</td>
                        <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_penugasan)); ?></td>
                        
                        <td class="project-actions text-right">
                            <a class="btn btn-primary btn-sm" href="/daftarpekerjaan/detil-pekerjaan/{{ $row->id }}" title="Detil">
                              <i class="fas fa-folder"></i>
                            
                            </a>
                            <!--
                            <a class="btn btn-info btn-sm" data-target="#modal-edit" >
                              <i class="fas fa-pencil-alt"></i>
                            </a>

                            <a class="btn btn-danger btn-sm" href="/daftarpekerjaan/hapus-pekerjaan/{{ $row->id }}">
                              <i class="fas fa-trash"></i>
                            </a>
                            -->
                            
                        </td>
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
        <!-- /.row -->


      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>
  
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        "buttons": ["excel", "pdf", "print"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

      $("#konfirmasi").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        "buttons": ["excel", "pdf", "print"]
      }).buttons().container().appendTo('#konfirmasi_wrapper .col-md-6:eq(0)');

      $("#pekerjaan").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        "buttons": ["excel", "pdf", "print"]
      }).buttons().container().appendTo('#pekerjaan_wrapper .col-md-6:eq(0)');
      
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "responsive": true,
         "targets": 'no-sort',
      });
    });


  </script>

@include('layouts/footer')
   
 
