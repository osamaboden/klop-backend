 <?PHP
  $menu = "Home";
  $page = "Dashboard";
  ?>

  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <h5 class="m-0">Order Baru</h5>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Tanggal Order</th>
                    <th>Id Order</th>
                    <th>Nama</th>
                    <th>Delivery</th>
                    <!-- <th>Status</th>  -->
                    <th class="aksi">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?PHP foreach($listorder as $row){ 
                    if(!empty($row['status']) && $row['status'] == 'baru' ){
                  ?>
                  <tr>
                    <td><?PHP echo date("d M Y H:i:s",strtotime($row['waktu_order'])); ?></td>
                    <td><?PHP echo $row['id']; ?></td>
                    <td><?PHP echo $row['nama_member']; ?></td>
                    <td><?PHP if($row['delivery'] == 'ya'){ echo"Diantar"; }else{ echo "Diambil";} ?></td>
                    <!--<td><?PHP echo $row['status']; ?></td>-->
                  
                    <td class="project-actions text-right">
                        <a class="btn btn-primary btn-sm" href="/order/detilorder/<?PHP echo $row['id']; ?>" title="Detil">
                          <i class="fas fa-folder"></i>
                        </a>
                    </td>
                  </tr>
                  <?PHP
                      }
                    }
                  ?>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
          <!-- /.col-md-6 -->

          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <h5 class="m-0">Menunggu Konfirmasi</h5>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Tanggal Order</th>
                    <th>Id Order</th>
                    <th>Nama</th>
                    <th>Delivery</th>
                    <!-- <th>Status</th> -->
                    <th class="aksi">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?PHP foreach($listorder as $row){ 
                    if(!empty($row['status']  && $row['status'] == 'menunggu konfirmasi')){
                  ?>
                  <tr>
                    <td><?PHP echo date("d M Y H:i:s",strtotime($row['waktu_order'])); ?></td>
                    <td><?PHP echo $row['id']; ?></td>
                    <td><?PHP echo $row['nama_member']; ?></td>
                    <td><?PHP if($row['delivery'] == 'ya'){ echo"Diantar"; }else{ echo "Diambil";} ?></td>
                    <!--<td><?PHP echo $row['status']; ?></td> -->
                  
                    <td class="project-actions text-right">
                        <a class="btn btn-primary btn-sm" href="/order" title="Detil">
                          <i class="fas fa-folder"></i>
                        </a>
                    </td>
                  </tr>
                  <?PHP
                      }
                    }
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->


          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h5 class="m-0">Pekerjaan</h5>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    
                    <th>Id Order</th>
                    <th>Waktu Order</th>
                    <th>Nama Karyawan</th>
                    <th>Waktu Penugasan</th>
                    <!-- <th>Kategori</th> -->
                   
                    <th class="aksi">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($listpekerjaan as $row)
                  <tr data-id="{{ $row->id }}" data-karyawan="{{ $row->id_karyawan }}" data-id_order="{{ $row->id_order }}" data-kategori="{{ $row->kategori }}" data-waktu_penugasan="<?PHP echo date('d-m-Y H:i:s',strtotime($row->waktu_penugasan)); ?>" data-status="{{ $row->status }}">
                    <td>{{ $row->id_order }}</td>
                    <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_order)); ?></td>
                    <td>{{ $row->nama_karyawan }}</td>
                    <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_penugasan)); ?></td>
                    
                    <td class="project-actions text-right">
                        <a class="btn btn-primary btn-sm" href="order/detilorder/{{ $row->id_order }}" title="Detil">
                          <i class="fas fa-folder"></i>
                        
                        </a>
                        <!--
                        <a class="btn btn-info btn-sm" data-target="#modal-edit" >
                          <i class="fas fa-pencil-alt"></i>
                        </a>

                        <a class="btn btn-danger btn-sm" href="/daftarpekerjaan/hapus-pekerjaan/{{ $row->id }}">
                          <i class="fas fa-trash"></i>
                        </a>
                        -->
                        
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- /.col-md-12 -->

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>
  
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        "buttons": ["excel", "pdf", "print"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "responsive": true,
         "targets": 'no-sort',
      });
    });


  </script>

@include('layouts/footer')
   
 
