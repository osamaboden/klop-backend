
<?PHP
$menu = "Karyawan";
$page = "Detil Karyawan";
?>
@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/member">Karyawan</a></li>
              <li class="breadcrumb-item active">Detil</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{ asset('storage/profile/karyawan') }}/{{$datakaryawan->foto_profile}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$datakaryawan->nama}}</h3>

                <p class="text-muted text-center">{{$datakaryawan->hak_akses}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item"><i class="fas fa-phone"></i>&nbsp;&nbsp;
                    {{$datakaryawan->no_hp}} <b><span class="float-right"></span></b>
                  </li>
                  <li class="list-group-item"><i class="far fa-envelope"></i>&nbsp;&nbsp;
                    {{$datakaryawan->email}} <b><span class="float-right"></span></b>
                  </li>
                  <li class="list-group-item"><i class="fas fa-building"></i>&nbsp;&nbsp;
                    {{$datakaryawan->nama_divisi}} <b><span class="float-right"></span></b>
                  </li>
                </ul>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-lg-9">

                      <!-- Form Element sizes -->
                      <div class="card card-primary card-outline">
                        <div class="card-header">
                          <h3 class="card-title">History Pekerjaan</h3>
                        </div>
                        <div class="card-body">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Id Order</th>
                              <th>Waktu Order</th>
                              <th>Waktu Penugasan</th>
                              <th>Kategori</th>
                              <th>Status</th>
                              <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?PHP foreach($pekerjaankaryawan as $row){ ?>
                            <tr>
                              <td>{{ $row->id_order }}</td>
                              <td><?PHP echo date('d M Y H:i:s', strtotime($row->waktu_order)); ?></td>
                              <td><?PHP echo date('d M Y H:i:s', strtotime($row->waktu_penugasan)); ?></td>
                              <td><?PHP echo $row->kategori; ?></td>
                               <td><?PHP echo $row->status; ?></td>
                              <td class="project-actions text-right">
                                <a class="btn btn-primary btn-sm" href="/order/detilorder/{{ $row->id_order }}" title="Detil">
                                  <i class="fas fa-folder"></i>
                                </a>
                                  <!--<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-produk-<?PHP echo $row->id_order; ?>">
                                    <i class="fas fa-folder"></i>
                                  </button> -->
                              </td>
                            </tr>
                            <?PHP } ?>
                            </tbody>
                            
                          </table>
                        </div>
                        <!-- /.card-body -->
                        
                      </div>
                      <!-- /.card -->
          </div>
        </div>
        <!-- / .row -->
       
        
         
      </div>
      <!-- /.container-fluid -->
      @foreach($alamatmember as $baris)
      <div class="modal fade" id="modal-default-{{$baris->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Alamat {{ $baris->nama }}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
                <label>Nama</label>
                <p>{{$baris->nama}}</p>                            
                                
                <label>Alamat</label>
                <p>{{ $baris->alamat }}</p>
                                
                <label>Kecamatan</label>
                <p>{{ $baris->nama_kecamatan }}</p>

                <label>Kabupaten</label>
                <p>{{ $baris->nama_kabupaten }}</p>

                <label>Provinsi</label>
                <p>{{ $baris->nama_provinsi }}</p>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      @endforeach
      
      <?PHP foreach($listorder['order'] as $item){ ?>
      <!-- modal produk -->
      <div class="modal fade" id="modal-produk-<?PHP echo $item['id_order']; ?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detail Order</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <label>Id Order </label>

                <?PHP  
                echo "<b>".$item['id_order']."</b>"; 
                ?>
                

                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th>Item</th>
                      <th style="width: 10px">Qty</th>
                      <th>Harga Satuan</th>
                      <th>Diskon</th>
                      <th>Jumlah</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?PHP 
                      $total = 0;
                      foreach($item['produk'] as $row){
                    ?>
                    
                    <tr>
                      <td><?PHP echo $row['nama_produk']; ?></td>
                      <td><?PHP echo $row['qty']; ?></td>
                      <td>
                        <?PHP
                          $harga_peritem = $row['harga_satuan'];
                          echo number_format($harga_peritem, 0, '', '.');
                        ?>
                      </td>
                      <td>
                        <?PHP
                        $diskon_peritem = $row['satuan_diskon'];
                        if($diskon_peritem == 'nominal'){
                          $diskon = $row['diskon_satuan'];
                          echo number_format($diskon, 0, '', '.');
                          
                        }else{
                          $diskon = ($row['diskon_satuan']/100)*($harga_peritem*$row['qty']);
                          echo number_format ($diskon, 0, '', '.');
                          echo ' ('.$row['diskon_satuan'].'%)';
                        }
                        ?>
                      </td>
                      <td align="right">
                        <?PHP
                        if($diskon_peritem == 'nominal'){
                          echo number_format(($row['harga_satuan']*$row['qty'])-$diskon, 0, '', '.');
                        }else{
                          
                          echo number_format($row['harga_satuan']*$row['qty'] - $diskon, 0, '', '.');
                        }
                        ?>
                      </td>
                    </tr>
                    <?PHP
                    $total += ($row['harga_satuan']*$row['qty'])-$diskon;
                  }
                    ?>
                    
                    
                    <tr>
                      <td colspan="4"><b>TOTAL</b></td>
                      <td align="right">
                      <?PHP
                       echo '<b> Rp '. number_format($total,0, '', '.').'<b/>';
                      ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
                
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal (modal produk)-->
      <?PHP
      }
      ?>

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script><!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/demo.js') }}"></script>


<!-- Page specific script -->
  <script>

    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        /*"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"] */
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>

@include('layouts/footer')
   
 
