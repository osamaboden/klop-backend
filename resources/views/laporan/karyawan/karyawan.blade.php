  <?PHP
  $menu = "Karyawan";
  $page = "Daftar Karyawan";
  ?>
  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>&nbsp;</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Karyawan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Karyawan</h3>
                <div class="text-right">
                  <!--
                  <a class="btn btn-info btn-sm" href="/member/tambah">
                      <i class="fas fa-plus"></i> Tambah
                  </a> -->
                  <button id="btnTambah" type="button" class="btn btn-info btn-sm" data-toggle="modal">
                    <i class="fas fa-plus"></i> Tambah
                  </button>
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Nama Karyawan</th>
                    <th>No Handphone</th>
                    <th>Email</th>
                    <th>Divisi</th>
                    <th>Hak Akses</th>
                    <th>Status</th>
                    <th class ="aksi">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($listkaryawan as $row)
                  <tr data-id="{{ $row->id }}" data-nama="{{ $row->nama }}" data-nohp="{{ $row->no_hp }}" data-email="{{ $row->email }}" data-iddivisi="{{$row->id_divisi}}" data-hakakses="{{$row->hak_akses}}" data-status="{{ $row->status }}" >
                    <td>{{ $row->nama }}</td>
                    <td>{{ $row->no_hp }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->nama_divisi }}</td>
                    <td>{{ $row->hak_akses }}</td>
                    <td>{{ $row->status }}</td>
                    <td class="project-actions text-right">
                        <a class="waiki btn btn-primary btn-sm" href="/karyawan/detil/{{ $row->id }}" id="waiki" title="Detil">
                          <i class="fas fa-folder"></i>
                        </a>
                        <!--
                        <a class="btn btn-info btn-sm" href="/member/edit/{{ $row->id }}">
                          <i class="fas fa-pencil-alt"></i> Ubah
                        </a> -->
                        <a class="btn btn-info btn-sm" title="Ubah" data-target="#modal-edit">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a class="btn btn-danger btn-sm" title="Hapus" href="/karyawan/hapus/{{ $row->id }}">
                          <i class="fas fa-trash"></i>
                        </a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                  
                </table>

              </div>
              <!-- /.card-body -->

            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->

      <!-- modal -->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <form action="/karyawan/simpan" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}            
              <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" required="required" name="nama" placeholder="Nama lengkap karyawan">
              </div>
              <div class="form-group">
                  <label>No. Handphone</label>
                  <input type="text" class="form-control" required="required" name="nohp" placeholder="No HP karyawan">
              </div>
              <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" required="required" name="email" placeholder="Alamat email karyawan">
              </div>
              <div class="form-group">
                <label>Divisi</label>
                <select name = "divisi" class="form-control">
                  @foreach($listdivisi as $baris)
                  <option value="{{$baris->id}}">{{$baris->nama}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Hak Akses</label>
                <select name="hak_akses" class="form-control">
                  <option value="admin">Admin</option>
                  <option value="karyawan">Karyawan</option>
                </select>
              </div>
              <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" required="required" name="Password awal untuk karyawan">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="aktif">Aktif</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- modal edit -->
      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <form action="/karyawan/update" method="post" >
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}   
              <input type="hidden" name="id">          
              <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" required="required" name="nama" placeholder="Enter ...">
              </div>
              <div class="form-group">
                  <label>No. Handphone</label>
                  <input type="text" class="form-control" required="required" name="nohp" placeholder="Enter ...">
              </div>
              <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" required="required" name="email" placeholder="Enter ...">
              </div>
              <div class="form-group">
                <label>Divisi</label>
                <select name = "divisi" class="form-control">
                  @foreach($listdivisi as $baris)
                  <option value="{{$baris->id}}">{{$baris->nama}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Hak Akses</label>
                <select name="hak_akses" class="form-control">
                  <option value="admin">Admin</option>
                  <option value="karyawan">Karyawan</option>
                </select>
              </div>
              <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" placeholder="di isi jika ingin ganti password">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="aktif">Aktif</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#example1 tbody').on('click', 'tr', function(){
        var id = $(this).attr('data-id');
        var nama = $(this).attr('data-nama');
        var no_hp = $(this).attr('data-nohp');
        var email = $(this).attr('data-email');
        var id_divisi = $(this).attr('data-iddivisi');
        var hak_akses = $(this).attr('data-hakakses');
        var status = $(this).attr('data-status');

            $('input[name="id"]').val(id);
            $('input[name="nama"]').val(nama);
            $('input[name="nohp"]').val(no_hp);
            $('input[name="email"]').val(email);
            $('select[name="divisi"]').val(id_divisi);
            $('select[name="hak_akses"]').val(hak_akses);
            $('select[name="status"]').val(status);
            $('#modal-edit').modal('show');
          
      });

      $('#btnTambah').on('click', function(){
          $('input[name="id"]').val('');
          $('input[name="nama"]').val('');
          $('input[name="nohp"]').val('');
          $('input[name="email"]').val('');
          $('select[name="hak_akses"]').val('admin');
           $('select[name="status"]').val('aktif');
          $('#modal-default').modal('show');
      });
  
    });
  </script>
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        "buttons": ["excel", "pdf", "print"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>

@include('layouts/footer')