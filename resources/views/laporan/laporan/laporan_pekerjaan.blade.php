  <?PHP
  $menu = "Laporan";
  $page = "Laporan Pekerjaan";
  ?>
  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>&nbsp;</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Home</a></li>
              <li class="breadcrumb-item active"><?PHP echo $page; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><?PHP echo $page; if(!empty($mulai) && !empty($selesai)){echo "&nbsp;".$mulai." s/d ".$selesai;} ?> </h3>
                <div class="text-right">
                </div>
              </div>
              <!-- /.card-header -->
              
              <div class="card-body">
                <div class="row">
                  <div class="col-md-3 offset-md-3">
                    <form action="/laporan-pekerjaan/by-tanggal" method="post">
                      {{ csrf_field() }}
                    <div class="form-group">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                        <input type="text" class="form-control float-right" name="bytanggal" id="reservation">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-md btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    </form>
                  </div>
                  

                  <div class="col-md-3">
                    <form action="/laporan-pekerjaan/by-karyawan" method="post">
                      {{ csrf_field() }}
                        <div class="input-group">
                          
                            <input type="search" class="form-control form-control-md" name="bykaryawan" placeholder="nama karyawan">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-md btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
                <br/>   
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    
                    <th>Id Order</th>
                    <th>Waktu Order</th>
                    <th>Status</th>
                    <th>Nama Karyawan</th>
                    <th>Waktu Penugasan</th>
                    <th>Pengiriman</th>
                    <th>Pemasangan</th>
                    <!--<th>Kategori</th> 
                   
                    <th class="aksi">Aksi</th> -->
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($listpekerjaan as $row)
                  <tr data-id="{{ $row->id }}" data-karyawan="{{ $row->id_karyawan }}" data-id_order="{{ $row->id_order }}" data-kategori="{{ $row->kategori }}" data-waktu_penugasan="<?PHP echo date('d-m-Y H:i:s',strtotime($row->waktu_penugasan)); ?>" data-status="{{ $row->status }}">
                    <td>{{ $row->id_order }}</td>
                    <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_order)); ?></td>
                    <td>{{ $row->status }}</td>
                    <td>{{ $row->nama_karyawan }}</td>
                    <td><?PHP echo date('d M Y H:i:s',strtotime($row->waktu_penugasan)); ?></td>
                    <td>{{ $row->delivery }}</td>
                    <td>{{ $row->installation }}</td>
                    
                  @endforeach
                  </tbody>
                  
                </table>

              </div>
              <!-- /.card-body -->

            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Bootstrap4 Duallistbox -->
  <script src="{{ asset('assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
  
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- Select2 -->
  <script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }} "></script>
  <!-- InputMask -->
  <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/inputmask/jquery.inputmask.min.js ') }}"></script>
  <!-- date-range-picker -->
  <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }} "></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){

      //Date range picker
      $('#reservation').daterangepicker({
        locale: {
            format: 'DD/MM/YYYY'
        }

      });
       
      
      
      //Date and time picker
      $('#reservationdatetime').datetimepicker({  
        format: 'DD-MM-YYYY HH:mm:ss',
        icons: { time: 'far fa-clock' }
       });

      $('#tgldanwaktu').datetimepicker({  
        format: 'DD-MM-YYYY HH:mm:ss',
        icons: { time: 'far fa-clock' }
       });
  
    });
  </script>
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "paging": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

      $('#example2').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });

        

    });
  </script>

@include('layouts/footer')