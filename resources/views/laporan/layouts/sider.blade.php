

  
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img src="{{ asset('assets/img/kloplogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">KLOP</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) 
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('storage/profile/karyawan/user.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/karyawan/detil/<?PHP echo session()->get('id'); ?>" class="d-block"><?PHP echo session()->get('nama'); ?></a>
        </div>
      </div>-->

      <!-- SidebarSearch Form 
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>-->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-flat" data-widget="treeview" role="menu" data-accordion="false"> 
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <!--
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Starter Pages
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Active Page</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inactive Page</p>
                </a>
              </li>
            </ul>
          </li> 
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Simple Link
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>-->

          <!--<li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Produk
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="/product" class="nav-link" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Produk</p>
                </a>
              </li>
            </ul>
          </li> -->

          <li class="nav-item">
            <a href="/home" class="nav-link <?PHP if($menu == 'Home'){ echo "active";} ?>">
              <i class="nav-icon fas fa-home"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <li class="nav-item">
                <a href="/order" class="nav-link <?PHP if($menu == 'Order'){ echo "active";} ?>">
                  <i class="nav-icon fas fa-shopping-basket"></i>
                  <p>Order</p>
                </a>
          </li>

          <li class="nav-item">
                <a href="/order/kofirmpembayaran" class="nav-link <?PHP if($menu == 'Pembayaran'){ echo "active";} ?>">
                  <i class="nav-icon fas fa-bullhorn"></i>
                  <p>Konfirmasi Pembayaran</p>
                </a>
          </li>

          <li class="nav-item">
            <a href="/member" class="nav-link <?PHP if($menu == 'Member'){ echo "active";} ?> ">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Member
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/product" class="nav-link <?PHP if($menu == 'Produk'){ echo "active";} ?>">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Produk
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/galeri" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>Galeri</p>
            </a>
          </li>

          <li class="nav-item" >
            <a href="/karyawan" class="nav-link <?PHP if($menu == 'Karyawan'){ echo "active";} ?>">
              <i class="nav-icon fas fa-user"></i>
              <p>Karyawan</p>
            </a>
          </li>

          <li class="nav-item" >
                <a href="/daftarpekerjaan" class="nav-link <?PHP if($menu == 'Pekerjaan'){ echo "active";} ?>">
                  <i class="fas fa-hard-hat nav-icon"></i>
                  <p>Daftar Pekerjaan</p>
                </a>
          </li>

          <li class="nav-item">
                <a href="/kendaraan" class="nav-link <?PHP if($menu == 'Kendaraan'){echo "active";} ?>" >
                  <i class="fas fa-truck nav-icon"></i>
                  <p>Kendaraan</p>
                </a>
          </li>

          <li class="nav-item">
                <a href="/slideshow" class="nav-link <?PHP if($menu == 'Slideshow'){ echo "active";} ?>">
                  <i class="fas fa-clone nav-icon"></i>
                  <p>Slideshow</p>
                </a>
          </li>

          <li class="nav-item">
                <a href="/konten" class="nav-link <?PHP if($menu == 'Konten'){ echo "active";} ?>">
                  <i class="fas fa-clone nav-icon"></i>
                  <p>Konten</p>
                </a>
          </li>

          <li class="nav-item <?PHP if($menu == 'Master Data'){ echo "menu-open";} ?>">
            <a href="#" class="nav-link <?PHP if($menu == 'Master Data'){ echo "active";} ?>">
              <i class="nav-icon fas fa-server"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/karyawandivisi" class="nav-link <?PHP if($page == 'Divisi Karyawan'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Divisi</p>
                </a>
              </li>
            </ul>
            <!--
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/kendaraan" class="nav-link <?PHP if($page == 'Daftar Kendaraan'){echo "active";} ?>" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kendaraan</p>
                </a>
              </li>
            </ul> -->
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/biaya-instalasi" class="nav-link <?PHP if($page == 'Biaya Instalasi'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Biaya Instalasi</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/proyekkategori" class="nav-link <?PHP if($page == 'Kategori Proyek'){ echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kategori Proyek</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item <?PHP if($menu == 'Laporan'){ echo "menu-open";} ?>">
            <a href="#" class="nav-link <?PHP if($menu == 'Laporan'){ echo "active";} ?>">
              <i class="nav-icon fas fa-file-contract"></i>
              <p>
                Laporan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/laporan-order" class="nav-link <?PHP if($page == 'Laporan Order'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Laporan Order</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/laporan-pekerjaan" class="nav-link <?PHP if($page == 'Laporan Pekerjaan'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Laporan Pekerjaan</p>
                </a>
              </li>
            </ul>
          </li>



          <!--<li class="nav-item <?PHP if($menu == 'Member'){ echo "menu-open";} ?>">
            <a href="#" class="nav-link <?PHP if($menu == 'Member'){ echo "active";} ?>">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Member
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="/member" class="nav-link <?PHP if($page == 'Daftar Member'){echo "active";} ?>" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Member</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="/order" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>History Order</p>
                </a>
              </li>
              
            </ul>
          </li>-->

          <!--<li class="nav-item <?PHP if($menu == 'Order'){ echo "menu-open";} ?>">
            <a href="#" class="nav-link <?PHP if($menu == 'Order'){ echo "active";} ?>">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Order
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/order" class="nav-link <?PHP if($page == 'Daftar Order'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Order</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/order/kofirmpembayaran" class="nav-link <?PHP if($page == 'Konfirmasi Pembayaran'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Konfirmasi Pembayaran</p>
                </a>
              </li>
              
            </ul>
          </li>-->


          <!--
          <li class="nav-item <?PHP if($menu == 'Karyawan'){ echo "menu-open";} ?>">
            <a href="#" class="nav-link <?PHP if($menu == 'Karyawan'){ echo "active";} ?>">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Karyawan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/karyawan" class="nav-link <?PHP if($page == 'Daftar Karyawan'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Karyawan</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/daftarpekerjaan" class="nav-link <?PHP if($page == 'Daftar Pekerjaan'){echo "active";} ?>" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Daftar Pekerjaan</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/karyawandivisi" class="nav-link <?PHP if($page == 'Divisi Karyawan'){echo "active";} ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Divisi</p>
                </a>
              </li> 
            </ul>
          </li>-->

          
          <!--
          <li class="nav-item" >
                <a href="/logout" class="nav-link <?PHP if($menu == 'logout'){ echo "active";} ?>">
                  <i class="fas fa-power-off nav-icon"></i>
                  <p>Logout</p>
                </a>
          </li> -->

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  