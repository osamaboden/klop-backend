<?PHP
  $menu = "Order";
  $page = "Detil Order";
?>

@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/order">Order</a></li>
              <li class="breadcrumb-item active">Detil</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-5">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              
              <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item"><i class="fas fa-qrcode"></i>
                    &nbsp; {{$detilorder->id}} <b><span class="float-right"></span></b>
                  </li>
                  <li class="list-group-item"><i class="fas fa-user"></i> 
                   &nbsp; {{$detilorder->nama_member}} <b><span class="float-right"></span></b>
                  </li>
                  <li class="list-group-item"><i class="far fa-clock"></i> 
                   &nbsp; <?PHP echo date('d M Y H:i:s',strtotime($detilorder->waktu_order)); ?> <b><span class="float-right"></span></b>
                  </li>
                  
                  <li class="list-group-item"><i class="fas fa-hard-hat"></i>
                    &nbsp; {{$detilorder->nama_proyek}} <b><span class="float-right"></span></b>
                  </li>
                  <!--
                  <li class="list-group-item"><i class="fas fa-money-bill"></i>
                    &nbsp; {{$detilorder->nama_instalasi}} [ <?PHP echo number_format ($detilorder->nilai_installation, 0, '', '.'); ?> ] <b><span class="float-right"></span></b>
                  </li>
                  -->
                  <!--
                  <li class="list-group-item">
                    <table>
                      <tr>
                        <td valign="top"><i class="fas fa-flask"></i></td>
                        <td>
                          <?PHP
                          foreach($orderstatus as $item){
                            echo "&nbsp; [ ".date('d M Y H:i:s',strtotime($item->waktu_perubahan))." ] [<b> ".$item->status." </b>] [ ".$item->nama_karyawan." ]<br/>";
                          }
                          ?>
                        </td>
                      </tr>
                    </table>
                    <b><span class="float-right"></span></b>
                  </li> -->
                </ul>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Pengiriman</h3>
              </div>
                <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item"><i class="fas fa-truck"></i>
                   &nbsp;
                   <?PHP 
                   $delivery = $detilorder->delivery;
                   if($delivery == 'ya'){
                    echo"Diantar";
                    echo" [ ".$detilorder->nama_kendaraan." ] ";
                   }else{
                    echo"Diambil";
                   }
                   ?>
                   <?PHP echo date('d M Y H:i:s',strtotime($detilorder->request_ready)); ?><b><span class="float-right"></span></b>
                  </li>
                  <li class="list-group-item"> 
                    <table border="0">
                      <tr>
                        <td valign="top">
                          <i class="fas fa-home"></i>&nbsp;
                        </td>
                        <td>
                          {{$detilorder->alamat}}, {{$detilorder->nama_kecamatan}}, {{$detilorder->nama_kabupaten}}, <br/> {{$detilorder->nama_provinsi}}
                        </td>
                      </tr>
                    </table>
                    <b><span class="float-right"></span></b>
                  </li>
                  <li class="list-group-item"><i class="fas fa-phone"></i> 
                   &nbsp; {{$detilorder->nomor_telepon}} <b><span class="float-right"></span></b>
                  </li>
                </ul>
                </div>
                 <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Status Pesanan</h3>
              </div>
              <div class="card-body ">
                
                  <!-- The timeline -->
                  <div class="timeline timeline-inverse">
                    
                    <?PHP
                        foreach($orderstatus as $item){
                    ?>
                          
                    <!-- timeline item -->
                    <div>
                      <i class="far fa-circle bg-gray"></i>

                      <div class="timeline-item">
                        <h4 class="timeline-header border-0">
                          <?PHP 
                          echo date('d M Y H:i:s',strtotime($item->waktu_perubahan))." | <b>".$item->status."</b>"; 
                          ?>
                        </h4>
                      </div>
                    </div>
                    <!-- END timeline item -->
                    
                    <?PHP
                    }
                    ?> 

                  </div>
                  <!-- end The timeline -->
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col-md-5 -->
          

          <div class="col-lg-7">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Pesanan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th>Item</th>
                      <th style="width: 10px">Qty</th>
                      <th>Harga Satuan (Rp)</th>
                      <th>Diskon</th>
                      <th>Jumlah (Rp)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?PHP 
                      $total = 0; 
                    ?>
                    @foreach($produkorder as $row)
                    <tr>
                      <td>{{$row->nama_produk}}</td>
                      <td>{{$row->qty}}</td>
                      <td>
                        <?PHP
                          echo number_format($row->harga_total, 0, '', '.');
                        ?>
                      </td>
                      <td>
                        <?PHP
                        if($row->satuan_diskon == 'nominal'){
                          echo number_format($row->diskon, 0, '', '.');
                          $diskon = $row->diskon;
                        }else{
                          $diskon = ($row->diskon/100)*($row->harga_total*$row->qty);
                          echo number_format ($diskon, 0, '', '.');
                          echo ' ('.$row->diskon.'%)';
                        }
                        ?>
                      </td>
                      <td align="right">
                        <?PHP
                        if($row->satuan_diskon == 'nominal'){
                          echo number_format(($row->harga_total*$row->qty)-$diskon, 0, '', '.');
                        }else{
                          
                          echo number_format($row->harga_total*$row->qty - $diskon, 0, '', '.');
                        }
                        ?>
                      </td>
                    </tr>
                    <?PHP $total += ($row->harga_total*$row->qty)-$diskon; ?>
                    
                    @endforeach
                    <tr>
                      <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="4">Biaya pemasangan ( {{$detilorder->nama_instalasi}} )</td>
                      <td align="right"><?PHP echo number_format ($detilorder->nilai_installation, 0, '', '.'); ?> </td>
                    </tr>
                    <tr>
                      <td colspan="4">Biaya pengiriman</td>
                      <td align="right"><?PHP echo number_format ($detilorder->nilai_delivery, 0, '', '.'); ?> </td>
                    </tr>
                    <tr>
                      <td colspan="4"><b>TOTAL</b></td>
                      <td align="right">
                      <?PHP
                       echo '<b>'. number_format($total+$detilorder->nilai_installation+$detilorder->nilai_delivery,0, '', '.').'<b/>';
                      ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            

            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Pembayaran</h3>
              </div>
              <div class="card-body ">
                <?PHP
                  if(!is_null($orderpembayaran)){
                ?>
                <div class="row">
                  <div class="col-md-3">

                      <p>
                        <?PHP 
                          if(!empty($orderpembayaran->bukti_transfer)){
                        ?>
                        <img src="{{ asset('storage/bukti_transfer')}}/{{$orderpembayaran->bukti_transfer}}" width="150px" class="" alt="User Image">
                        <?PHP
                        }
                        ?>
                      </p>
                  </div>
                  <div class="col-md-9">
                      <label>Bank</label>
                      <p><?PHP echo $orderpembayaran->bank; ?></p>

                      <label>Rekening Atas Nama</label>
                      <p><?PHP echo $orderpembayaran->atas_nama; ?></p>

                      <label>Nominal Transfer (Rp)</label>
                      <p><?PHP echo number_format($orderpembayaran->nominal_transfer, 0, '', '.'); ?></p>

                      <label>Status</label>
                      <p>
                        <?PHP echo $orderpembayaran->status; ?>
                      </p>
                  </div>
                </div>
                <!-- /end row -->
                <?PHP
                  }else{echo"Menunggu pembayaran";}
                ?>
                  
              </div>
            </div>

          </div>
          <!-- / .col-lg-9 -->
        </div>
        <!-- / .row -->
       
        
         
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/demo.js') }}"></script>

@include('layouts/footer')
   
 
