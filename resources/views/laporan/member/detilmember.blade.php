<?PHP
  $menu = "Member";
  $page = "Detil Member";
?>

@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/member">Member</a></li>
              <li class="breadcrumb-item active">Detil</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{ asset('assets/img/user4-128x128.jpg') }}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$datamember->nama}}</h3>

                <p class="text-muted text-center">akun</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item"><i class="fas fa-phone"></i>&nbsp;
                    {{$datamember->no_hp}} <b><span class="float-right"></span></b>
                  </li>
                  <li class="list-group-item"><i class="far fa-envelope"></i>&nbsp;
                    {{$datamember->email}} <b><span class="float-right"></span></b>
                  </li>
                  <!--
                  @foreach($alamatmember as $baris)
                  <li class="list-group-item">
                  <a href="" data-toggle="modal" data-target="#modal-default-{{$baris->id}}">
                  <i class="fas fa-home"></i>&nbsp;
                  {{$baris->nama}}
                  </a>
                  </li>
                  @endforeach
                  -->

                </ul>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--
          <div class="col-lg-9">

                      <-- Form Element sizes --
                      <div class="card card-primary card-outline">
                        <div class="card-header">
                          <h3 class="card-title">History Order</h3>
                        </div>
                        <div class="card-body">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Waktu</th>
                              <th>Id Order</th>
                              <th>Delivery</th>
                              <th>Status</th>
                              <th class="aksi">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?PHP foreach($listorder['order'] as $row){ ?>
                            <tr>
                              <td><?PHP echo date('d M Y H:i:s', strtotime($row['waktu_order']));  ?></td>
                              <td><?PHP echo $row['id_order']; ?></td>
                              <--<td><?PHP echo $row['alamat'].$row['nama_kecamatan'].','.$row['nama_kabupaten'].','.$row['nama_provinsi']; ?></td> ->
                              <td><?PHP if($row['delivery'] == 'ya'){echo "Diantar";}else{echo"Diambil";} ?></td>
                              <td><?PHP echo $row['status'];  ?></td>
                              <td class="project-actions text-right">
                                  
                                  <a class="btn btn-primary btn-sm" href="/order/detilorder/<?PHP echo $row['id_order']; ?>">
                                    <i class="fas fa-folder"></i>
                                  </a>
                                  <--<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-produk-<?PHP echo $row['id_order']; ?>" title="Lihat"><i class="fas fa-folder"></i>
                                  </button>
                                  
                                  <a class="btn btn-info btn-sm" href="/member/edit/<?PHP echo $row['id_order'] ?>">
                                    <i class="fas fa-pencil-alt"></i> Ubah
                                  </a>
                                  <a class="btn btn-danger btn-sm" href="/member/hapus/<?PHP echo $row['id_order'] ?>">
                                    <i class="fas fa-trash"></i> hapus
                                  </a>->
                              </td>
                            </tr>
                            <?PHP } ?>
                            </tbody>
                          </table>
                        </div>
                        <-- /.card-body ->
                        
                      </div>
                      <-- /.card ->
          </div> -->

          <div class="col-8 col-sm-12 col-md-8">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-order" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Alamat </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-menunggu" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Histori Order</a>
                  </li>
                  <!--
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-pekerjaan" role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Video</a>
                  </li>
                  -->
                </ul>
              </div>

              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <!-- Alamat -->
                  <div class="tab-pane fade show active" id="custom-tabs-four-order" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                      <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Alamat</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($alamatmember as $baris)
                            <tr>
                              <td>{{$baris->nama}}</td>
                              <td>{{ $baris->alamat }}, {{ $baris->nama_kecamatan }}, {{ $baris->nama_kabupaten }}, {{ $baris->nama_provinsi }} </td>
                            </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>

                  <!-- histori order -->
                  <div class="tab-pane fade" id="custom-tabs-four-menunggu" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                    <table id="histori-order" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Waktu</th>
                        <th>Id Order</th>
                        <th>Delivery</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?PHP foreach($listorder['order'] as $row){ ?>
                            <tr>
                              <td><?PHP echo date('d M Y H:i:s', strtotime($row['waktu_order']));  ?></td>
                              <td><?PHP echo $row['id_order']; ?></td>
                              <!--<td><?PHP echo $row['alamat'].$row['nama_kecamatan'].','.$row['nama_kabupaten'].','.$row['nama_provinsi']; ?></td> -->
                              <td><?PHP if($row['delivery'] == 'ya'){echo "Diantar";}else{echo"Diambil";} ?></td>
                              <td><?PHP echo $row['status'];  ?></td>
                              <td class="project-actions text-right">
                                  
                                  <a class="btn btn-primary btn-sm" href="/order/detilorder/<?PHP echo $row['id_order']; ?>">
                                    <i class="fas fa-folder"></i>
                                  </a>
                                  <!--<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-produk-<?PHP echo $row['id_order']; ?>" title="Lihat"><i class="fas fa-folder"></i>
                                  </button>
                                  
                                  <a class="btn btn-info btn-sm" href="/member/edit/<?PHP echo $row['id_order'] ?>">
                                    <i class="fas fa-pencil-alt"></i> Ubah
                                  </a>
                                  <a class="btn btn-danger btn-sm" href="/member/hapus/<?PHP echo $row['id_order'] ?>">
                                    <i class="fas fa-trash"></i> hapus
                                  </a>-->
                              </td>
                            </tr>
                            <?PHP } ?>
                      </tbody>
                    </table>
                  </div>

                  <!-- video 
                  <div class="tab-pane fade" id="custom-tabs-four-pekerjaan" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
                    abcd
                  </div>-->

                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>

        </div>
        <!-- / .row -->
       
        
         
      </div>
      <!-- /.container-fluid 
      @foreach($alamatmember as $baris)
      <div class="modal fade" id="modal-default-{{$baris->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Alamat {{ $baris->nama }}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
                <label>Nama</label>
                <p>{{$baris->nama}}</p>                            
                                
                <label>Alamat</label>
                <p>{{ $baris->alamat }}</p>
                                
                <label>Kecamatan</label>
                <p>{{ $baris->nama_kecamatan }}</p>

                <label>Kabupaten</label>
                <p>{{ $baris->nama_kabupaten }}</p>

                <label>Provinsi</label>
                <p>{{ $baris->nama_provinsi }}</p>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <-- /.modal-content ->
        </div>
        <-- /.modal-dialog ->
      </div>
      <-- /.modal ->
      @endforeach -->
      
      <?PHP foreach($listorder['order'] as $item){ ?>
      <!-- modal produk -->
      <div class="modal fade" id="modal-produk-<?PHP echo $item['id_order']; ?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detail Order</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <label>Id Order </label>

                <?PHP  
                echo "<b>".$item['id_order']."</b>"; 
                ?>
                

                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th>Item</th>
                      <th style="width: 10px">Qty</th>
                      <th>Harga Satuan</th>
                      <th>Diskon</th>
                      <th>Jumlah</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?PHP 
                      $total = 0;
                      foreach($item['produk'] as $row){
                    ?>
                    
                    <tr>
                      <td><?PHP echo $row['nama_produk']; ?></td>
                      <td><?PHP echo $row['qty']; ?></td>
                      <td>
                        <?PHP
                          $harga_peritem = $row['harga_total'];
                          echo number_format($harga_peritem, 0, '', '.');
                        ?>
                      </td>
                      <td>
                        <?PHP
                        $diskon_peritem = $row['satuan_diskon'];
                        if($diskon_peritem == 'nominal'){
                          $diskon = $row['diskon'];
                          echo number_format($diskon, 0, '', '.');
                          
                        }else{
                          $diskon = ($row['diskon']/100)*($harga_peritem*$row['qty']);
                          echo number_format ($diskon, 0, '', '.');
                          echo ' ('.$row['diskon'].'%)';
                        }
                        ?>
                      </td>
                      <td align="right">
                        <?PHP
                        if($diskon_peritem == 'nominal'){
                          echo number_format(($row['harga_total']*$row['qty'])-$diskon, 0, '', '.');
                        }else{
                          
                          echo number_format($row['harga_total']*$row['qty'] - $diskon, 0, '', '.');
                        }
                        ?>
                      </td>
                    </tr>
                    <?PHP
                    $total += ($row['harga_total']*$row['qty'])-$diskon;
                  }
                    ?>
                    
                    
                    <tr>
                      <td colspan="4"><b>TOTAL</b></td>
                      <td align="right">
                      <?PHP
                       echo '<b> Rp '. number_format($total,0, '', '.').'<b/>';
                      ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
                
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal (modal produk)-->
      <?PHP
      }
      ?>

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script><!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/demo.js') }}"></script>


<!-- Page specific script -->
  <script>

    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

      $("#histori-order").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#histori-order_wrapper .col-md-6:eq(0)');

      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>

@include('layouts/footer'); 
   
 
