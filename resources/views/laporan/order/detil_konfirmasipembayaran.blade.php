<?PHP
  $menu = "Pembayaran";
  $page = "Detil Pembayaran";
?>

@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Home</a></li>
              <li class="breadcrumb-item active"><a href="/order/kofirmpembayaran">Konfirmasi Pembayaran</a></li>
              <li class="breadcrumb-item active">Detil</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          
          

          <div class="col-lg-12">
            
            

            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title"><?PHP echo "#".$orderpembayaran->id_order; ?></h3>
              </div>
              <div class="card-body ">
                <?PHP
                  if(!is_null($orderpembayaran)){
                ?>
                <div class="row">
                  <div class="col-md-2">
                      <p>
                        
                        <img src="{{ asset('storage/bukti_transfer')}}/{{$orderpembayaran->bukti_transfer}}" width="150px" class="" alt="User Image">
                        
                      </p>
                  </div>
                  <div class="col-md-10">
                      <label>Bank</label>
                      <p><?PHP echo $orderpembayaran->bank; ?></p>

                      <label>Rekening Atas Nama</label>
                      <p><?PHP echo $orderpembayaran->atas_nama; ?></p>

                      <label>Nominal Transfer (Rp)</label>
                      <p><?PHP echo number_format($orderpembayaran->nominal_transfer, 0, '', '.'); ?></p>

                      <label>Status</label>
                      <p>
                        <?PHP echo $orderpembayaran->status; ?>
                      </p>
                  </div>
                </div>
                <!-- /end row -->
                <?PHP
                  }else{echo"Menunggu pembayaran";}
                ?>
                  
              </div>
            </div>

          </div>
          <!-- / .col-lg-9 -->
        </div>
        <!-- / .row -->
       
        
         
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/demo.js') }}"></script>

@include('layouts/footer')
   
 
