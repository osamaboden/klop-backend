
@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Tambah Data Produk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/product">Produk</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <form action="/product/simpan" method="post">
        <div class="row">
          <div class="col-lg-5">
              
              {{ csrf_field() }}
                      <input type="hidden" name="id" value="@if($id != '') {{ $dataproduk->id }} @endif "> <br/>
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Produk</h3>
                        </div>
                        <div class="card-body">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Nama Produk</label>
                              <input type="text" class="form-control" required="required" name="nama" value = "@if($id != '') {{ $dataproduk->nama }} @endif" placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>Deskripsi</label>
                              <textarea class="form-control" rows="3" required="required" name="deskripsi" placeholder="Enter ...">@if($id != '') {{ $dataproduk->deskripsi }} @endif</textarea>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="aktif" @if($id != '' && $dataproduk->status == "aktif" ) checked @endif >
                                    <label class="form-check--label">Aktif</label>
                                  </div>
                               </div>
                               <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="nonaktif" @if($id != '' && $dataproduk->status == "nonaktif" ) checked @endif>
                                    <label class="form-check--label">Non Aktif</label>
                                  </div>
                               </div>
                              </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-info">Simpan</button>
                        </div>
                      </div>
                      <!-- /.card -->
             
          </div>

          <div class="col-lg-7">
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Galeri</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                              <label>Judul</label>
                              <input type="text" class="form-control" required="required" name="judul" placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>Kategori</label>
                              <select name="kategori" class="form-control">
                                <option value="0">--Kategori--</option>
                                <option value="video">Video</option>
                                <option value="image">Gambar</option>
                              </select>
                            </div>
                            <div class="custom-file">
                              <label>Image</label>
                              <input name="namafile" type="file" class="custom-file-input" id="customFile">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <div class="custom-file">
                              <label>Image</label>
                              <input type="file" class="custom-file-input" id="customFile">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <div class="custom-file">
                              <label>Image</label>
                              <input type="file" class="custom-file-input" id="customFile">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <div class="custom-file">
                              <label>Image</label>
                              <input type="file" class="custom-file-input" id="customFile">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <div class="custom-file">
                              <label>Image</label>
                              <input type="file" class="custom-file-input" id="customFile">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <div class="custom-file">
                              <label>Image</label>
                              <input type="file" class="custom-file-input" id="customFile">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="aktif">
                                    <label class="form-check--label">Aktif</label>
                                  </div>
                               </div>
                               <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="nonaktif">
                                    <label class="form-check--label">Non Aktif</label>
                                  </div>
                               </div>
                              </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-info">Simpan</button>
                        </div>
                      </div>
                      <!-- /.card -->
              
          </div>
           <!-- /.col-md-6 -->
        </div>
        <!-- / .row -->
       
        <br/>
        <div><button type="submit" class="btn btn-info">Simpan</button></div>
        </form>
         
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('layouts/footer'); 
   
 
