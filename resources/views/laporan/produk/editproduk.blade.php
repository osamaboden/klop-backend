 <?PHP
  $menu = "Produk";
  $page = "Edit Produk";
  ?>

@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="/product">Produk</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <form action="/product/update" method="post">
        <div class="row">
          <div class="col-lg-5">
              {{ csrf_field() }}
                      <input type="hidden" name="id" value="{{ $dataproduk->id }} "> 
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Produk</h3>
                        </div>
                        <div class="card-body">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Nama Produk</label>
                              <input type="text" class="form-control" required="required" name="nama" placeholder="Enter ..." value="{{ $dataproduk->nama }}">
                            </div>
                            <div class="form-group">
                              <label>Deskripsi</label>
                              <textarea class="form-control" rows="3" required="required" name="deskripsi" placeholder="Enter ...">{{ $dataproduk->deskripsi }}</textarea>
                            </div>
                            <div class="form-group">
                              <label>Status</label>
                              <select class="form-control" name="status">
                                <option value="aktif" @if($dataproduk->status == "aktif" ) selected @endif >Aktif</option>
                                <option value="tidak" @if($dataproduk->status == "tidak" ) selected @endif >Tidak</option>
                              </select>
                            </div>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
          </div>
          <div class="col-lg-7">
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Harga</h3>
                        </div>
                        <div class="card-body">
                          <div class="row">
                            <?PHP
                              foreach ($produkharga as $key => $item) {
                            ?>
                            <div class="col-sm-5">
                              <div class="form-group">
                                <label>Qty</label>
                                <input type="text" class="form-control" name="qty[<?PHP echo $key; ?>]" value="<?PHP echo $item->qty; ?>">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Harga</label>
                                <input type="text" class="form-control" name="harga[<?PHP echo $key; ?>]" value="<?PHP echo $item->harga; ?>">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_harga[<?PHP echo $key; ?>]">
                                  <option value="aktif" <?PHP if($item->status == 'aktif'){echo "selected";} ?> >Aktif</option>
                                  <option value="tidak" <?PHP if($item->status == 'tidak'){echo "selected";} ?> >Tidak</option>
                                </select>
                              </div>
                            </div>
                            <?PHP
                              }
                            ?>
                            
                            <div class="col-sm-5">
                              <div class="form-group">
                                <label>Qty</label>
                                <input type="text" class="form-control" name="qty00" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Harga</label>
                                <input type="text" class="form-control" name="harga00" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_harga00">
                                  <option value="aktif">Aktif</option>
                                  <option value="tidak">Tidak</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->  
          </div>

          <div class="col-lg-12">
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Galeri</h3>
                        </div>
                        <div class="card-body">
                          <div class="row">
                            
                            <?PHP
                              foreach ($produkgaleri as $no => $row) {
                                if($row->kategori == 'image'){
                            ?>
                              <!-- /.form foto -->
                              <div class="col-sm-5">
                                <div class="form-group">
                                  <label>Judul</label>
                                  <input type="text" class="form-control" required="required" name="judul" value="<?PHP echo $row->title; ?>">
                                </div>
                              </div>
                              <input type="hidden" name = "kategori" value="image">
                              
                              <div class="col-sm-5">
                                  <label>Foto</label>
                                  <input type="hidden" name="namafile_hidden" value="<?PHP echo $row->nama_file; ?>">
                                  <div class="custom-file">
                                    <input name="namafile" type="file" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                  </div>
                              </div>
                              <div class="col-sm-2">
                                <div class="form-group">
                                  <label>Status</label>
                                  <select class="form-control" name="status_galeri">
                                    <option value="aktif" <?PHP if($row->title == 'aktif'){echo "selected"; } ?> >Aktif</option>
                                    <option value="tidak" <?PHP if($row->title == 'tidak'){echo "selected"; } ?> >Tidak</option>
                                  </select>
                                </div>
                              </div>
                              <!-- /.form foto -->

                            <?PHP
                                }else{
                            ?>

                              <!-- form video -->
                              <div class="col-sm-5">
                                <div class="form-group">
                                  <label>Judul</label>
                                  <input type="text" class="form-control" name="judul_video" value="<?PHP echo $row->title; ?>">
                                </div>
                              </div>
                              <input type="hidden" name = "kategori_galeri_video" value="video">
                              <div class="col-sm-5">
                                  <input type="hidden" name = "url_hidden" value="video">
                                  <label>Alamat URL</label>
                                  <input type="text" class="form-control" required="required" name="url" value="<?PHP echo $row->nama_file; ?>">
                              </div>
                              <div class="col-sm-2">
                                <div class="form-group">
                                  <label>Status</label>
                                  <select class="form-control" name="status_galeri_video">
                                    <option value="aktif" <?PHP if($row->title == 'aktif'){echo "selected"; } ?> >Aktif</option>
                                    <option value="tidak" <?PHP if($row->title == 'tidak'){echo "selected"; } ?> >Tidak</option>
                                  </select>
                                </div>
                              </div>
                              <!-- /.form video -->
                            <?PHP
                                 
                                }
                              }
                            ?>

                            <!-- ============================================================================================= -->
                            
                            
                          </div>
                        </div>
                        <!-- /.card body -->
                      </div> 
          </div>
          <div class="col-12">
            <input type="submit" value="Simpan" class="btn btn-success float-right">
          </div>

        </div>
        <!-- / .row -->
        </form>
        <br/>
         
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/demo.js') }}"></script>

@include('layouts/footer'); 
   
 
