<?PHP
  $menu = "Produk";
  $page = "Tambah Produk";
?>
@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/product">Produk</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <form action="/product/simpan" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="col-lg-5">
              
              {{ csrf_field() }}
                      
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Produk</h3>
                        </div>
                        <div class="card-body">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Nama Produk</label>
                              <input type="text" class="form-control" required="required" name="nama" placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>Deskripsi</label>
                              <textarea class="form-control" rows="3" required="required" name="deskripsi" placeholder="Enter ..."></textarea>
                            </div>
                            <div class="form-group">
                              <label>Status</label>
                              <select class="form-control" name="status_produk">
                                <option value="aktif">Aktif</option>
                                <option value="tidak">Tidak</option>
                              </select>
                            </div>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
             
          </div>
          
          <div class="col-lg-7">
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Harga</h3>
                          <button id="btnTambah" type="button" class="btn btn-info btn-sm float-right" data-toggle="modal">
                          <i class="fas fa-plus"></i> Tambah
                          </button>
                        </div>
                        <div class="card-body">
                         
                          <table class="table table-sm" id="tabel-harga">
                            <thead>
                              <tr>
                                <th>Qty</th>
                                <th>Harga (Rp)</th>
                                <th>Status</th>
                                <th>aksi</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr id="kosong">
                                    <td colspan="5" align="center">Belum ada data</td>
                                </tr>
                            </tbody>
                          </table>
                            <!--
                          <div class="row">
                            <?PHP
                            for($i=0;$i<=3;$i++){
                            ?>
                            <div class="col-sm-5">
                              <div class="form-group">
                                <label>Qty</label>
                                <input type="text" class="form-control" name="qty[<?PHP echo $i; ?>]" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label>Harga</label>
                                <input type="text" class="form-control" name="harga[<?PHP echo $i; ?>]" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_harga[<?PHP echo $i; ?>]">
                                  <option value="aktif">Aktif</option>
                                  <option value="tidak">Tidak</option>
                                </select>
                              </div>
                            </div>
                            <?PHP
                            }
                            ?>
                          </div> -->
                          <!-- /.row -->
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->  
          </div>
          <div class="col-lg-12">
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Galeri</h3>
                          <button id="tambahVideo" type="button" class="btn btn-info btn-sm float-right" data-toggle="modal">
                          <i class="fas fa-plus"></i> Video
                          </button>

                          <button id="tambahFoto" type="button" class="btn btn-info btn-sm float-right" data-toggle="modal">
                          <i class="fas fa-plus"></i> Foto
                          </button>
                        </div>
                        <div class="card-body">
                          <div class="row">

                            <table class="table table-sm" id="tabel-videofoto">
                            <thead>
                              <tr>
                                <th>Judul</th>
                                <th>Video / Foto</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr id="kosong">
                                    <td colspan="5" align="center">Belum ada data</td>
                                </tr>
                            </tbody>
                          </table>

                            <!-- 
                            <div class="col-sm-5">
                              <div class="form-group">
                                <label>Judul</label>
                                <input type="text" class="form-control" required="required" name="judul" placeholder="Enter ...">
                              </div>
                            </div>
                            <input type="hidden" name = "kategori" value="image">
                            
                            <div class="col-sm-5">
                                <label>Foto</label>
                                <div class="custom-file">
                                  <input name="namafile" type="file" class="custom-file-input" id="customFile">
                                  <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                              <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_galeri">
                                  <option value="aktif">Aktif</option>
                                  <option value="tidak">Tidak</option>
                                </select>
                              </div>
                            </div>
                            
                            <div class="col-sm-5">
                              <div class="form-group">
                                <label>Judul</label>
                                <input type="text" class="form-control" name="judul_videoXXXX" placeholder="Enter ...">
                              </div>
                            </div>
                            <input type="hidden" name = "kategori_galeri_video" value="video">
                            
                            <div class="col-sm-5">
                                <label>Alamat URL</label>
                                <input type="text" class="form-control" required="required" name="url" placeholder="Enter ...">
                            </div>
                            <div class="col-sm-2">
                              <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status_galeri_video">
                                  <option value="aktif">Aktif</option>
                                  <option value="tidak">Tidak</option>
                                </select>
                              </div>
                            </div>
                            -->

                            
                          </div>
                        </div>
                        <!-- /.card body -->
                      </div> 
          </div>     
       
          <div class="col-12">
            <input type="submit" value="Simpan" class="btn btn-success float-right">
          </div>
        
       </div>
        <!-- / .row -->
        </form>
         <br/>
      </div><!-- /.container-fluid -->
      <!-- modal -->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Harga</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
                       
              <div class="form-group">
                  <label>Qty</label>
                  <input type="number" class="form-control" required="required" name="qty" placeholder="Enter ...">
              </div>
              <div class="form-group">
                  <label>Harga</label>
                  <input type="text" class="form-control" required="required" name="harga" placeholder="Enter ...">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status_harga" class="form-control">
                  <option value="aktif">Aktif</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary float-right" id="BtnTambahkan" >Tambah</button>
            </div>
          </div>
          <!-- /.modal-content -->
          
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <!-- tambah video -->
      <!-- modal -->
      <div class="modal fade" id="modal-video">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Video</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
                       
              <div class="form-group">
                  <label>Judul</label>
                  <input type="text" class="form-control" required="required" name="judul_video" placeholder="Enter ...">
              </div>
              <div class="form-group">
                  <label>URL</label>
                  <input type="text" class="form-control" required="required" name="url_video" placeholder="Enter ...">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status_video" class="form-control">
                  <option value="aktif">Aktif</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary float-right" id="tambahkanVideo" >Tambah</button>
            </div>
          </div>
          <!-- /.modal-content -->
          
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- tambah foto -->
      <!-- modal -->
      <div class="modal fade" id="modal-foto">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Foto</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
                       
              <div class="form-group">
                  <label>Judul</label>
                  <input type="text" class="form-control" required="required" name="judul_foto" placeholder="Enter ...">
              </div>
              <div class="form-group">
                  <label>Gambar</label>
                  <input type="file" id="file" name="gambar_produk" value="PILIH" />
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status_foto" class="form-control">
                  <option value="aktif">Aktif</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary float-right" id="tambahkanFoto" >Tambah</button>
            </div>
          </div>
          <!-- /.modal-content -->
          
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

  <script type="text/javascript">
  $(document).ready(function(){
    $('#btnTambah').on('click', function(){
          $('input[name="id"]').val('');
          $('input[name="qty"]').val('');
          $('input[name="harga"]').val('');
           $('select[name="status"]').val('0');
          $('#modal-default').modal('show');
    });

    $('#BtnTambahkan').on('click', function(){
        var qty = $('input[name="qty"]').val();
        var harga = $('input[name="harga"]').val();
        var status = $('select[name="status_harga"] option:selected').val('aktif');

        var qtyview = parseInt(qty);
        var hargaview = parseInt(harga);
          
          var baru = '<tr id ="hapusdah'+ qty +'" data-qty="'+ qty +'" data-harga="'+ harga +'" data-status="'+ status +'"><td>' + qtyview.toLocaleString("de") + '<input type="hidden" name="qty[]" value="' + qty + '" /></td><td align="center">' + hargaview.toLocaleString("de") + '<input type="hidden" name="harga[]" value="' + harga + '" /></td><td>' + status + '<input type="hidden" name="status_harga[]" value="' + status + '" /></td><td><a class="btn btn-info btn-sm" data-target="#modal-edit" title="Ubah"><i class="fas fa-pencil-alt"></i>ubah</a></td></tr>';

         
          $('#tabel-harga tbody tr#kosong').remove();
          $('#tabel-harga').append(baru);
          
          $('#modal-default').modal('hide');
    });

    
    $('#tabel-harga tbody').on('click', 'tr', function(){
        
        var qty = $(this).attr('data-qty');
        var harga = $(this).attr('data-harga');
        var status = $(this).attr('data-status');
        $('#hapusdah'+qty).remove();


            
            $('input[name="qty"]').val(qty);
            $('input[name="harga"]').val(harga);
            $('select[name="status_harga"]').val(status);
            $('#modal-default').modal('show');
          
    });



    $('#tambahVideo').on('click', function(){
          $('input[name="id"]').val('');
          $('input[name="judul_video"]').val('');
          $('input[name="url_video"]').val('');
          $('select[name="status_video"]').val('aktif');
          $('#modal-video').modal('show');
    });

    $('#tambahkanVideo').on('click', function(){
        var judul_video = $('input[name="judul_video"]').val();
        var url_video = $('input[name="url_video"]').val();
        var status_video = $('select[name="status_video"] option:selected').val();
        var gambar = $('input[name="gambar_produk"]').val();

       
          
          var baru = '<tr><td>' + judul_video + '<input type="hidden" name="judul_video[]" value="' + judul_video + '" /></td><td align="center"><video width="320" height="240" controls><source src="' + url_video + '"></video><input type="hidden" name="url_video[]" value="' + url_video + '" /></td><td>' + status_video + '<input type="hidden" name="status_video[]" value="' + status_video + '" /></td></tr>';

         
          $('#tabel-videofoto tbody tr#kosong').remove();
          $('#tabel-videofoto').append(baru);
          
          $('#modal-video').modal('hide');
      });


    /* ============================================foto ===================== */
    $('#tambahFoto').on('click', function(){
          $('input[name="id"]').val('');
          $('input[name="judul_foto"]').val('');
          $('select[name="status_foto"]').val('aktif');
          $('#modal-foto').modal('show');
    });

    $('#tambahkanFoto').on('click', function(){
        var judul_foto = $('input[name="judul_foto"]').val();
       
        var status_foto = $('select[name="status_foto"] option:selected').val();
        var gambar = $('input[name="gambar_produk"]').val();

       
          
          var baru = '<tr><td>' + judul_foto + '<input type="hidden" name="judul_foto" value="' + judul_foto + '" /></td><td align="center"><input type="file" accept="image/*" onchange="readURL(event)" name="namafile"><br><img id="output" width="320px" name="namafile"></td><td>' + status_foto + '<input type="hidden" name="status_foto" value="' + status_foto + '" /></td></tr>';

         
          $('#tabel-videofoto tbody tr#kosong').remove();
          $('#tabel-videofoto').append(baru);
          
          $('#modal-foto').modal('hide');
      });

    var readURL= function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('output');
      output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
  
    };

  });
   
  </script>

@include('layouts/footer'); 
   
 
