<?PHP
  $menu = "Order";
  $page = "Detil Order";
?>

@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/order">Order</a></li>
              <li class="breadcrumb-item active">Detil</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
       
        <div class="row">
          <div class="col-5 col-sm-5">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Customer</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                
                <strong>Id Order</strong>
                <p class="text-muted"> {{$detilorder->id}} </p>
                
                <hr>
                <strong>Nama</strong>
                <p class="text-muted"> {{$detilorder->nama_member}} </p>

                <hr>
                <strong>Waktu Order</strong>
                <p class="text-muted"> <?PHP echo date('d M Y H:i:s',strtotime($detilorder->waktu_order)); ?> </p>

                <hr>
                <strong>Kategori Proyek</strong>
                <p class="text-muted"> {{$detilorder->nama_proyek}} </p>
                
                <hr>
                <strong>Pengiriman</strong>
                <p class="text-muted"> 
                  <?PHP 
                   $delivery = $detilorder->delivery;
                   if($delivery == 'ya'){
                    echo"Diantar";
                    echo" [ ".$detilorder->nama_kendaraan." ] ";
                   }else{
                    echo"Diambil";
                   }
                  ?>
                </p>

                <hr>
                <strong>Alamat</strong>
                <p class="text-muted">
                  {{$detilorder->alamat}}, {{$detilorder->nama_kecamatan}}, {{$detilorder->nama_kabupaten}},  {{$detilorder->nama_provinsi}}
                </p>

                <hr>
                <strong>No. Telp</strong>
                <p class="text-muted">{{$detilorder->nomor_telepon}} </p>

                <hr>
                <strong>Status Order</strong>
                <p>
                <!-- The timeline -->
                <div class="timeline timeline-inverse">
                    
                  <?PHP
                      foreach($orderstatus as $item){
                  ?>
                          
                  <!-- timeline item -->
                  <div>
                    <i class="far fa-circle bg-gray"></i>

                    <div class="timeline-item">
                      <h4 class="timeline-header border-0">
                        <?PHP 
                        echo date('d M Y H:i:s',strtotime($item->waktu_perubahan))." | <b>".$item->status."</b>"; 
                        $status_pengirman = $item->status;
                        ?>
                      </h4>
                    </div>
                  </div>
                  <!-- END timeline item -->
                    
                  <?PHP
                  }
                  ?> 

                </div>
                <!-- end The timeline -->
                </p>
                <?PHP
                  if($status_pengirman <> 'selesai'){
                ?>
                <hr>
                <p>
                  <button id="btnTambah" type="button" class="btn btn-info btn-block" data-toggle="modal">
                    <b>Penugasan</b>
                  </button>
                </p>
                <?PHP
                  }
                ?>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-7 col-sm-7">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-pesanan" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Pesanan</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-pembayaran" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Pembayaran</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  
                  <!-- pesanan -->
                  <div class="tab-pane fade show active" id="custom-tabs-four-pesanan" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                    <table class="table table-sm">
                      <thead>
                        <tr>
                          <th>Item</th>
                          <th style="width: 10px">Qty</th>
                          <th>Harga Satuan (Rp)</th>
                          <th>Diskon</th>
                          <th>Jumlah (Rp)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?PHP 
                          $total = 0; 
                        ?>
                        @foreach($produkorder as $row)
                        <tr>
                          <td>{{$row->nama_produk}}</td>
                          <td>{{$row->qty}}</td>
                          <td>
                            <?PHP
                              echo number_format($row->harga_total, 0, '', '.');
                            ?>
                          </td>
                          <td>
                            <?PHP
                            if($row->satuan_diskon == 'nominal'){
                              echo number_format($row->diskon, 0, '', '.');
                              $diskon = $row->diskon;
                            }else{
                              $diskon = ($row->diskon/100)*($row->harga_total*$row->qty);
                              echo number_format ($diskon, 0, '', '.');
                              echo ' ('.$row->diskon.'%)';
                            }
                            ?>
                          </td>
                          <td align="right">
                            <?PHP
                            if($row->satuan_diskon == 'nominal'){
                              echo number_format(($row->harga_total*$row->qty)-$diskon, 0, '', '.');
                            }else{
                              
                              echo number_format($row->harga_total*$row->qty - $diskon, 0, '', '.');
                            }
                            ?>
                          </td>
                        </tr>
                        <?PHP $total += ($row->harga_total*$row->qty)-$diskon; ?>
                        
                        @endforeach
                        <tr>
                          <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="4">Biaya pemasangan ( {{$detilorder->nama_instalasi}} )</td>
                          <td align="right"><?PHP echo number_format ($detilorder->nilai_installation, 0, '', '.'); ?> </td>
                        </tr>
                        <tr>
                          <td colspan="4">Biaya pengiriman</td>
                          <td align="right"><?PHP echo number_format ($detilorder->nilai_delivery, 0, '', '.'); ?> </td>
                        </tr>
                        <tr>
                          <td colspan="4"><b>TOTAL</b></td>
                          <td align="right">
                          <?PHP
                           echo '<b>'. number_format($total+$detilorder->nilai_installation+$detilorder->nilai_delivery,0, '', '.').'<b/>';
                          ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <!-- pembayaran -->
                  <div class="tab-pane fade" id="custom-tabs-four-pembayaran" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">

                    <?PHP
                      if(!is_null($orderpembayaran)){
                    ?>
                    <div class="row">
                      <div class="col-md-3">

                          <p>
                            <?PHP 
                              if(!empty($orderpembayaran->bukti_transfer)){
                            ?>
                            <img src="{{ asset('storage/bukti_transfer')}}/{{$orderpembayaran->bukti_transfer}}" width="150px" class="" alt="User Image">
                            <?PHP
                            }
                            ?>
                          </p>
                      </div>
                      <div class="col-md-9">
                          <label>Bank</label>
                          <p><?PHP echo $orderpembayaran->bank; ?></p>

                          <label>Rekening Atas Nama</label>
                          <p><?PHP echo $orderpembayaran->atas_nama; ?></p>

                          <label>Nominal Transfer (Rp)</label>
                          <p><?PHP echo number_format($orderpembayaran->nominal_transfer, 0, '', '.'); ?></p>

                          <label>Waktu Konfirmasi</label>
                          <p><?PHP echo date('d M Y H:i:s',strtotime($orderpembayaran->waktu_konfirmasi)); ?></p>

                          <label>Status</label>
                          <p>
                            <?PHP echo $orderpembayaran->status; ?>
                          </p>
                      </div>
                    </div>
                    <!-- /end row -->
                    <?PHP
                      }else{echo"Menunggu pembayaran";}
                    ?>
                    
                  </div>

                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>

        </div>
        <!-- / .row -->

        
         
      </div><!-- /.container-fluid -->

        <!-- modal -->
        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <form action="/daftarpekerjaan/simpan-pekerjaan" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambah</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body"> 
              {{ csrf_field() }}
                <div class="form-group">
                    <label>Id Order </label>
                    <input type="hidden" name="id_order" value="{{$detilorder->id}}">
                    <input type="text" class="form-control" required="required" name="id_order_readonly" readonly="readonly" value="{{$detilorder->id}}">
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <select name = "status" class="form-control">
                    <option value="baru">Baru</option>
                    <option value="dikerjakan">Dikerjakan</option>
                    <option value="selesai">Selesai</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nama Karyawan</label>
                  <select name="karyawan" class="form-control">
                    <?PHP
                    foreach ($listkaryawan as $item) {
                    ?>
                    <option value="<?PHP echo $item->id; ?>"><?PHP echo $item->nama; ?></option>
                    <?PHP
                    }
                    ?>
                  </select>
                </div>
                <!-- Date and time -->
                <div class="form-group">
                  <label>Waktu Penugasan</label>
                    <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" name="waktu_penugasan" data-target="#reservationdatetime"/>
                      <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                  <label>Pekerjaan</label>
                  <select name="kategori" class="form-control">
                    <option value="delivery">Delivery</option>
                    <option value="installation">Instalation</option>
                  </select>
                </div>
                <!-- /.form group -->
                
              </div>
              <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
            </form>
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>

<!-- =========================================== date picker ========================================= -->
<!-- InputMask -->
<script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/plugins/inputmask/jquery.inputmask.min.js ') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }} "></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- =========================================== date picker ========================================== -->

<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/demo.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
      
      $('#btnTambah').on('click', function(){
          $('input[name="id"]').val('');
          $('select[name="status"]').val('baru');
          $('#modal-default').modal('show');
      });

      //Date and time picker
      $('#reservationdatetime').datetimepicker({  
        format: 'DD-MM-YYYY HH:mm:ss',
        icons: { time: 'far fa-clock' }
       });

      $('#tgldanwaktu').datetimepicker({  
        format: 'DD-MM-YYYY HH:mm:ss',
        icons: { time: 'far fa-clock' }
       });
  
    });
  </script>

@include('layouts/footer')
   
 
