<?PHP
  $menu = "Member";
  $page = "Edit Member";
?>

@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/member">Member</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
              <form action="/member/update" method="post">
              {{ csrf_field() }}
                      <input type="hidden" name="id" value="{{ $datamember->id }} "> 
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Member</h3>
                        </div>
                        <div class="card-body">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Nama</label>
                              <input type="text" class="form-control" required="required" name="nama" value="{{ $datamember->nama }} " placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>No. Handphone</label>
                              <input type="text" class="form-control" required="required" name="nohp" value="{{ $datamember->no_hp }} " placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>Email</label>
                              <input type="text" class="form-control" required="required" name="email" value="{{ $datamember->email }} " placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="aktif" @if($datamember->status == "aktif" ) checked @endif >
                                    <label class="form-check--label">Aktif</label>
                                  </div>
                               </div>
                               <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="tidak" @if($datamember->status == "tidak" ) checked @endif>
                                    <label class="form-check--label">Tidak</label>
                                  </div>
                               </div>
                              </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-info">Simpan</button>
                        </div>
                      </div>
                      <!-- /.card -->
                </form>
          </div>
        </div>
        <!-- / .row -->
       
        
         
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/js/demo.js') }}"></script>

@include('layouts/footer'); 
   
 
