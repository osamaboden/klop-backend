<?PHP
  $menu = "Member";
  $page = "Tambah Member";
?>

@include('layouts/header')
@include('layouts/topnavbar')
@include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/member">Member</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <form action="/member/simpan" method="post">
        <div class="row">
          <div class="col-lg-5">
              
              {{ csrf_field() }}
                      
                      <!-- Form Element sizes -->
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Akun</h3>
                        </div>
                        <div class="card-body">
                          <!-- text input -->
                            <div class="form-group">
                              <label>Nama</label>
                              <input type="text" class="form-control" required="required" name="nama" placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>No. Handphone</label>
                              <input type="text" class="form-control" required="required" name="nohp" placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>Email</label>
                              <input type="text" class="form-control" required="required" name="email" placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="aktif">
                                    <label class="form-check--label">Aktif</label>
                                  </div>
                               </div>
                               <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="tidak" >
                                    <label class="form-check--label">Tidak</label>
                                  </div>
                               </div>
                              </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-info">Simpan</button>
                        </div>
                      </div>
                      <!-- /.card -->
             
          </div>

          <!-- form alamat member
          <div class="col-lg-7">
                     
                      <div class="card card-secondary">
                        <div class="card-header">
                          <h3 class="card-title">Alamat</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                              <label>Nama</label>
                              <input type="text" class="form-control" required="required" name="nama" placeholder="Enter ...">
                            </div>
                            <div class="form-group">
                              <label>Alamat</label>
                              <textarea class="form-control" rows="3" required="required" name="alamat" placeholder="Enter ..."></textarea>
                            </div>
                            <div class="form-group">
                              <label>Provinsi</label>
                              <select name="provinsi" class="form-control">
                                <option value="0">Provinsi</option>
                                <option value="video">Video</option>
                                <option value="image">Gambar</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label>Kabupaten</label>
                              <select name="kabupaten" class="form-control">
                                <option value="0">Kabupaten</option>
                                <option value="video">Video</option>
                                <option value="image">Gambar</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label>Kecamatan</label>
                              <select name="kecamatan" class="form-control">
                                <option value="0">Kecamatan</option>
                                <option value="video">Video</option>
                                <option value="image">Gambar</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label>Catatan</label>
                              <textarea class="form-control" rows="3" required="required" name="catatan" placeholder="Enter ..."></textarea>
                            </div>
                            <div class="form-group">
                              <label>Kordinat</label>
                              <textarea class="form-control" rows="3" required="required" name="koordinat" placeholder="Enter ..."></textarea>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="aktif">
                                    <label class="form-check--label">Aktif</label>
                                  </div>
                               </div>
                               <div class="col-sm-3">
                                  <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="nonaktif">
                                    <label class="form-check--label">Non Aktif</label>
                                  </div>
                               </div>
                              </div>
                            </div>
                        </div>
                        
                        <div class="card-footer">
                          <button type="submit" class="btn btn-info">Simpan</button>
                        </div>
                      </div>
                    
              
          </div> -->
           

          
        </div>
        <!-- / .row -->
       
        </form>
      </div><!-- /.container-fluid -->

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

@include('layouts/footer'); 
   
 
