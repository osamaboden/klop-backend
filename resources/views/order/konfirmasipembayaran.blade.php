  <?PHP
  $menu = "Pembayaran";
  $page = "Konfirmasi Pembayaran";
  ?>
  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>&nbsp;</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Konfirmasi Pembayaran</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Konfirmasi Pembayaran</h3>
                <div class="text-right">
                  <!--
                  <a class="btn btn-info btn-sm" href="/member/tambah">
                      <i class="fas fa-plus"></i> Tambah
                  </a> -->
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id Order</th>
                    <th>Waktu Konfirmasi</th>
                    <th>Nilai Order (Rp)</th>
                    <th>Status</th>
                    <th class="aksi">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($konfirmPembayaran as $row)
                  <tr>
                    <td>{{ $row->id_order}}</td>
                    <!-- <td>{{ $row->nama_member }}</td> -->
                    <!--<td><?PHP echo  date('d M Y H:i:s', strtotime($row->waktu_order)); ?></td>-->
                    <td><?PHP echo  date('d M Y H:i:s', strtotime($row->waktu_konfirmasi)); ?></td>
                    <td><?PHP echo number_format($row->nominal_transfer, 0, '', '.'); ?></td>
                    <td>{{ $row->status}}</td>
                    <td class="project-actions text-right">
                       <!-- <a class="btn btn-primary btn-sm" href="/order/detilpembayaran/{{ $row->id_order }}" title="Lihat">
                          <i class="fas fa-folder"></i>
                        </a> -->
                        <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-detil-{{$row->id_order}}" title="Detil">
                          <i class="fas fa-folder"></i>
                        </a>

                        <a class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-default-{{$row->id_order}}" title="Ubah">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                        <!--<a class="btn btn-danger btn-sm" href="/member/hapus/{{ $row->id }}">
                          <i class="fas fa-trash"></i> hapus
                        </a>-->
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>

              </div>
              <!-- /.card-body -->

            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->
      @foreach($konfirmPembayaran as $row)
      
      <div class="modal fade" id="modal-default-{{$row->id_order}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ubah</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="/order/ubahkonfirmasi" method="post" >
                {{ csrf_field() }} 
                <input type="hidden" name="id_konfirmasi" value="{{ $row->id}}">          
                <div class="form-group">
                  <label>Id Order</label>
                  <input type="text" class="form-control" name="nama" readonly="readonly" value="{{ $row->id_order}}">
                  <input type="hidden" class="form-control" name="id_order_hidden" value="{{ $row->id_order}}">
                </div>
                <!--
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" readonly="readonly" value="{{ $row->nama_member}}">
                </div> 
                <div class="form-group">
                  <label>Waktu Order</label>
                  <input type="text" class="form-control" name="nama" readonly="readonly" value="<?PHP echo  date('d M Y H:i:s', strtotime($row->waktu_order)); ?>">
                </div> -->
                <div class="form-group">
                  <label>Waktu Konfirmasi</label>
                  <input type="text" class="form-control" name="nama" readonly="readonly" value="<?PHP echo  date('d M Y H:i:s', strtotime($row->waktu_konfirmasi)); ?>">
                </div>
                <div class="form-group">
                  <label>Nominal Order (Rp)</label>
                  <input type="text" class="form-control" name="nama" readonly="readonly" value="<?PHP echo number_format($row->nominal_transfer, 0, '', '.'); ?>">
                </div>
                <!--
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea class="form-control" readonly="readonly">{{ $row->alamat}}, {{$row->nama_kecamatan}}, {{$row->nama_kabupaten}}, {{$row->nama_provinsi}}
                  </textarea>
                </div>
                -->
                <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" name="status">
                    <option value="Baru" @if( $row->status == 'Baru') selected @endif >Baru</option>
                    <option value="Lunas" @if( $row->status == 'Lunas') selected @endif>Lunas</option>
                    <option value="Tidak terkonfirmasi" @if( $row->status == 'Tidak Terkonfirmasi') selected @endif>Tidak Terkonfirmasi</option>
                  </select>
                </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      
      @endforeach


      @foreach($konfirmPembayaran as $row)
      
      <div class="modal fade" id="modal-detil-{{$row->id_order}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detil Pembayaran</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <label>Id Order</label>
              <p>{{ $row->id_order}}</p>

              <label>Waktu Konfirmasi</label>
              <p><?PHP echo  date('d M Y H:i:s', strtotime($row->waktu_konfirmasi)); ?></p>

              <label>Nominal Transfer (Rp)</label>
              <p><?PHP echo number_format($row->nominal_transfer, 0, '', '.'); ?></p>

              <label>Status</label>
              <p>
                <?PHP echo $row->status; ?>
              </p>

              <p>
                <img src="{{ asset('storage/bukti_transfer')}}/{{$row->bukti_transfer}}" width="150px" class="" alt="User Image">
              </p>
            </div>
            <!--
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div> -->
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      
      @endforeach

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>
  
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });


  </script>

@include('layouts/footer')