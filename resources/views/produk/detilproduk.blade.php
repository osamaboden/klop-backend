 <?PHP
  $menu = "Produk";
  $page = "Detil Produk";
  ?>

  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">&nbsp;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Home</a></li>
              <li class="breadcrumb-item"><a href="/product">Produk</a></li>
              <li class="breadcrumb-item active"><?PHP echo $page; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        
        <!-- ======================= -->
        <div class="row">
          <div class="col-4 col-sm-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Produk</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                
                <strong>Nama</strong>
                <p class="text-muted">
                 {{$detilproduk->nama}}
                </p>

                <hr>
                <strong>Deskripsi</strong>
                <p class="text-muted">
                   {{$detilproduk->deskripsi}}
                </p>

                <hr>
                 <strong>Status</strong>
                <p class="text-muted">
                  {{$detilproduk->status}}
                </p>

                <hr>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-8 col-sm-8">
            <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-order" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Harga Produk</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-menunggu" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Foto</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-pekerjaan" role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Video</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-four-order" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                     <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Qty</th>
                        <th>Harga (Rp)</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?PHP foreach($detilharga_produk as $row){ 
                      ?>
                      <tr>
                        <td><?PHP echo $row->qty; ?></td>
                        <td><?PHP echo number_format ($row->harga, 0, '', '.'); ?></td>
                      </tr>
                      <?PHP 
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>

                  <!-- foto -->
                  <div class="tab-pane fade" id="custom-tabs-four-menunggu" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                    <div class="row">
                      <?PHP 
                        foreach($detilgaleri_produk as $row){
                          if($row->kategori == 'image'){
                      ?>
                      <div class="col-sm-2">
                        <a href="{{asset('storage/galeri')}}/{{$row->nama_file}}" data-toggle="lightbox" data-title="{{$row->title}}" data-gallery="gallery">
                          <img src="{{asset('storage/galeri')}}/{{$row->nama_file}}" class="img-fluid mb-2" alt="white sample"/>
                        </a>
                      </div>
                      <?PHP 
                          } 
                        }
                      ?>
                    </div>
                  </div>

                  <!-- video -->
                  <div class="tab-pane fade" id="custom-tabs-four-pekerjaan" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
                    <div class="row">
                      <?PHP 
                        foreach($detilgaleri_produk as $row){
                          if($row->kategori == 'video'){
                      ?>
                      <div class="col-sm-2">
                        <a href="{{asset('storage/galeri')}}/{{$row->nama_file}}" data-toggle="lightbox" data-title="{{$row->title}}" data-gallery="gallery">
                          <img src="{{$row->thumbnail}}" class="img-fluid mb-2" alt="white sample"/>
                        </a>
                      </div>
                      <?PHP 
                          } 
                        }
                      ?>
                    </div>
                  </div>

                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
        <!-- /.row -->


      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- date-range-picker -->
  <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }} "></script>
  <!-- Ekko Lightbox -->
  <script src="{{ asset('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>
  
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        "buttons": ["excel", "pdf", "print"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": false,
        "autoWidth": false,
        "responsive": true,
         "targets": 'no-sort',
      });
    });

    $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })

  </script>

@include('layouts/footer')
   
 
