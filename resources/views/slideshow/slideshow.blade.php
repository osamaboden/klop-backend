  <?PHP
  $menu = "Slideshow";
  $page = "Slideshow";
  ?>
  @include('layouts/header')
  @include('layouts/topnavbar')
  @include('layouts/sider')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>&nbsp;</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
              <li class="breadcrumb-item active">Slideshow</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Slideshow</h3>
                <div class="text-right">
                  <!--
                  <a class="btn btn-info btn-sm" href="/member/tambah">
                      <i class="fas fa-plus"></i> Tambah
                  </a> -->
                  <button id="btnTambah" type="button" class="btn btn-info btn-sm" data-toggle="modal">
                    <i class="fas fa-plus"></i> Tambah
                  </button>
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Gambar</th>
                    <th>Tampilkan</th>
                    <th>Kategori</th>
                    <!-- <th class="aksi">Aksi</th> -->
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($listslideshow as $row)
                  <tr data-id="{{ $row->id }}" data-nama="{{ $row->gambar }}" data-status="{{ $row->status }}" data-kategori="{{ $row->kategori }}" >
                    <td>
                      <img src="{{asset('storage/corousel')}}/{{ $row->gambar }}" width="100px" class=" elevation-2" alt="corousel">
                    </td>
                    <td>{{ $row->status }}</td>
                    <td>{{ $row->kategori }}</td>
                     <!--<td class="project-actions text-right">
                        <a class="btn btn-info btn-sm" data-target="#modal-edit" title="Ubah">
                          <i class="fas fa-pencil-alt"></i>
                        </a>
                       
                        <a class="btn btn-danger btn-sm" href="/slideshow/hapus/{{ $row->id }}" title="hapus">
                          <i class="fas fa-trash"></i>
                        </a> 
                    </td>-->
                  </tr>
                  @endforeach
                  </tbody>
                  
                </table>

              </div>
              <!-- /.card-body -->

            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->

      <!-- modal -->
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <form action="/slideshow/simpan" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}            
              <div class="form-group">
                  <label>Gambar</label>
                  <input type="file" class="form-control" required="required" name="gambar" placeholder="Enter ...">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="ya">Ya</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              <div class="form-group">
                <label>Kategori</label>
                <select name = "kategori" class="form-control">
                  <option value="web">Web</option>
                  <option value="mobile">Mobile</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- modal edit -->
      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <form action="/slideshow/update" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"> 
            {{ csrf_field() }}   
              <input type="hidden" name="id">          
              <div class="form-group">
                  <label>Gambar</label>
                  <!--<p>
                  <div id='forimage'></div>
                  <img src="{{asset('storage/corousel')}}/{{ $row->gambar }}" width="100px" class=" elevation-2" alt="User Image">
                  </p> -->
                  <input type="file" class="form-control" name="gambar">
                  <input type="hidden" class="form-control" name="namafile_hidden">
              </div>
              
              <div class="form-group">
                <label>Status</label>
                <select name = "status" class="form-control">
                  <option value="ya">Ya</option>
                  <option value="tidak">Tidak</option>
                </select>
              </div>
              <div class="form-group">
                <label>Kategori</label>
                <select name = "kategori" class="form-control">
                  <option value="web">Web</option>
                  <option value="mobile">Mobile</option>
                </select>
              </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

    </section>
    <!-- /.content -->

  </div>
  <!-- /.content-wrapper -->

  <!-- jQuery -->
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- DataTables  & Plugins -->
  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('assets/js/demo.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#example1 tbody').on('click', 'tr', function(){
        var id = $(this).attr('data-id');
        var gambar= $(this).attr('data-nama');
        var status = $(this).attr('data-status');
        var kategori = $(this).attr('data-kategori');

            $('input[name="id"]').val(id);
            $('input[name="namafile_hidden"]').val(gambar);
            $('select[name="status"]').val(status);
            $('select[name="kategori"]').val(kategori);
            
            $('#modal-edit').modal('show');
          
      });

      $('#btnTambah').on('click', function(){
          $('input[name="id"]').val('');
          $('input[name="gambar"]').val('');
          $('select[name="status"]').val('ya');
          $('select[name="kategori"]').val('web');
           
          $('#modal-default').modal('show');
      });
  
    });
  </script>
  <!-- Page specific script -->
  <script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print"]
        //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>

@include('layouts/footer')