<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/datatabel',[Library::class,'datatabel'])->name('index');
Route::get('/form',[Library::class,'form'])->name('form');

//login & home
Route::get('/',[Dashboard::class,'index'])->name('index');
Route::post('/ceklogin',[Dashboard::class,'ceklogin'])->name('ceklogin');
Route::get('/logout',[Dashboard::class,'logout'])->name('logout');
Route::get('/home',[Dashboard::class,'home'])->name('home');

//produk
Route::get('/product',[Produk::class,'daftarproduk'])->name('index');
Route::get('/product/edit/{id}',[Produk::class,'edit'])->name('edit');
Route::post('/product/update',[Produk::class,'update'])->name('update');
Route::get('/product/hapus/{id}',[Produk::class,'hapus'])->name('hapus');
Route::get('/product/tambah/',[Produk::class,'tambah'])->name('tambah');
Route::post('/product/simpan',[Produk::class,'simpan'])->name('simpan');
Route::get('/product/detil-produk/{id}',[Produk::class,'detilProduk'])->name('detil');

//member
Route::get('/member',[Member::class,'daftarmember'])->name('index');
Route::get('/member/tambah',[Member::class,'tambah'])->name('tambah');
Route::post('/member/simpan',[Member::class,'simpan'])->name('simpan');
Route::get('/member/edit/{id}',[Member::class,'edit'])->name('edit');
Route::post('/member/update',[Member::class,'update'])->name('update');
Route::get('/member/hapus/{id}',[Member::class,'hapus'])->name('hapus');
Route::get('/member/detil/{id}',[Member::class,'detil'])->name('detil');

//order
Route::get('/order',[Order::class,'order'])->name('order');
Route::get('/order/detilorder/{id}',[Order::class,'detilorder'])->name('detilorder');
Route::get('/order/kofirmpembayaran',[Order::class,'kofirmasipembayaran'])->name('kofirmasipembayaran');
Route::post('/order/ubahkonfirmasi',[Order::class,'ubahkonfirmasi'])->name('ubahkonfirmasi');
Route::get('/order/detilpembayaran/{id}',[Order::class,'detilpembayaran'])->name('detilpembayaran');

//karyawan
Route::get('/karyawan',[Karyawan::class,'index'])->name('index');
Route::post('/karyawan/simpan',[Karyawan::class,'simpan'])->name('simpan');
Route::post('/karyawan/update',[Karyawan::class,'update'])->name('update');
Route::get('/karyawan/hapus/{id}',[Karyawan::class,'hapus'])->name('hapus');
Route::get('/karyawan/detil/{id}',[Karyawan::class,'detil'])->name('detil');

//dafatar pekerjaan
Route::get('/daftarpekerjaan',[Karyawan::class,'daftarpekerjaan'])->name('daftarpekerjaan');
Route::post('/daftarpekerjaan/simpan-pekerjaan',[Karyawan::class,'simpanpekerjaan'])->name('simpanpekerjaan');
Route::post('/daftarpekerjaan/update-pekerjaan',[Karyawan::class,'updatepekerjaan'])->name('updatepekerjaan');
Route::get('/daftarpekerjaan/hapus-pekerjaan/{id}',[Karyawan::class,'hapuspekerjaan'])->name('hapuspekerjaan');
Route::get('/daftarpekerjaan/detil-pekerjaan/{id}',[Karyawan::class,'detilpekerjaan'])->name('detilpekerjaan'); 

//====================================== MASTER DATA ==========================================
//kategori proyek
Route::get('/proyekkategori',[KategoriProyek::class,'kategoriproyek'])->name('index');
Route::post('/proyekkategori/simpan',[KategoriProyek::class,'simpan'])->name('simpan');
Route::post('/proyekkategori/update',[KategoriProyek::class,'update'])->name('update');
Route::get('/proyekkategori/hapus/{id}',[KategoriProyek::class,'hapus'])->name('hapus');

//Divisi Karyawan
Route::get('/karyawandivisi',[Karyawan::class,'divisikaryawan'])->name('index');
Route::post('/karyawandivisi/simpan',[Karyawan::class,'simpandivisi'])->name('simpan');
Route::post('/karyawandivisi/update',[Karyawan::class,'updatedivisi'])->name('update');
Route::get('/karyawandivisi/hapus/{id}',[Karyawan::class,'hapusdivisi'])->name('hapus');

//Kendaraan
Route::get('/kendaraan',[Kendaraan::class,'kendaraan'])->name('index');
Route::post('/kendaraan/simpan',[Kendaraan::class,'simpan'])->name('simpan');
Route::post('/kendaraan/update',[Kendaraan::class,'update'])->name('update');
Route::get('/kendaraan/hapus/{id}',[Kendaraan::class,'hapus'])->name('hapus');

//biaya instalasi
Route::get('/biaya-instalasi',[BiayaInstalasi::class,'biayainstalasi'])->name('index');
Route::post('/biaya-instalasi/simpan',[BiayaInstalasi::class,'simpan'])->name('simpan');
Route::post('/biaya-instalasi/update',[BiayaInstalasi::class,'update'])->name('update');
Route::get('/biaya-instalasi/hapus/{id}',[BiayaInstalasi::class,'hapus'])->name('hapus');

//homepage atau slide show
Route::get('/slideshow',[Slideshow::class,'index'])->name('index');
Route::post('/slideshow/simpan',[Slideshow::class,'simpan'])->name('simpan');
Route::post('/slideshow/update',[Slideshow::class,'update'])->name('update');
Route::get('/slideshow/hapus/{id}',[Slideshow::class,'hapus'])->name('hapus');

//====================================== MASTER DATA ==========================================

//Laporan
Route::get('/laporan-order',[Laporan::class,'laporanOrder'])->name('laporan order');
Route::post('/laporan-order/by-tanggal',[Laporan::class,'searchByTanggal'])->name('laporan by tanggal');
Route::post('/laporan-order/by-jumlah',[Laporan::class,'searchByJumlah'])->name('laporan by jumlah');
Route::post('/laporan-order/by-kategori',[Laporan::class,'searchByKategori'])->name('laporan by kategori');

Route::get('/laporan-pekerjaan',[Laporan::class,'laporanPekerjaan'])->name('laporan pekerjaan');
Route::post('/laporan-pekerjaan/by-karyawan',[Laporan::class,'cariByKaryawan'])->name('search by karyawan');
Route::post('/laporan-pekerjaan/by-tanggal',[Laporan::class,'cariByTanggal'])->name('search by karyawan');


//Galeri
Route::get('/galeri',[Galeri::class,'galeri'])->name('galeri');

//konten
Route::get('/konten',[Konten::class,'konten'])->name('konten');
Route::get('/konten/edit/{id}',[Konten::class,'editKonten'])->name('edit konten');
Route::post('/konten/simpan',[Konten::class,'simpanKonten'])->name('simpan konten');
